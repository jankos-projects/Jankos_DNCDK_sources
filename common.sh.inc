
die() {
    echo Error
    exit 1
}

JJ_LIB_DEST_ROOT="../../Jankos_DNCDK/jdncdk1"
SDCC_LIB_SRC_LOC="../../sdcc_lib_src"

THISNINJA="build/build_$THISLIB.ninja"

jjasm() {
    ALL_ASM="$ALL_ASM \$builddir/$1.rel \$\n"
    echo "build \$builddir/$1.rel: as $1.s" >>"$THISNINJA"
}


jjasmcrt() {
    echo "build \$builddir/$THECRT0.rel: as $THECRT0.s" >>"$THISNINJA"
}


jjasm_sdccls() {
    ALL_ASM="$ALL_ASM \$builddir/$1.rel \$\n"
    echo "build \$builddir/$1.rel: as \$sdcc_lib_src_dir/z80n/$1.s" >>"$THISNINJA"
}


jjcc() {
    ALL_CC="$ALL_CC \$builddir/$1.rel \$\n"
    echo "build \$builddir/$1.rel: cc3m $1.c" >>"$THISNINJA"
    #echo "build \$builddir/$1.rel: cc $1.c" >>"$THISNINJA"
}

jjcc2() {
    ALL_CC="$ALL_CC \$builddir/$1.rel \$\n"
    echo "build \$builddir/$1.rel: cc2 $1.c" >>"$THISNINJA"
}

jjcc3M(){
    ALL_CC="$ALL_CC \$builddir/$1.rel \$\n"
    echo "build \$builddir/$1.rel: cc3m $1.c" >>"$THISNINJA"
}

jjcc_sdccls2() {
    ALL_CC="$ALL_CC \$builddir/$1.rel \$\n"
    echo "build \$builddir/$1.rel: cc2 \$sdcc_lib_src_dir/$1.c" >>"$THISNINJA"
}

jjcc_sdccls() {
    ALL_CC="$ALL_CC \$builddir/$1.rel \$\n"
    echo "build \$builddir/$1.rel: cc \$sdcc_lib_src_dir/$1.c" >>"$THISNINJA"
}


generate_header() {
    printf "\nthislib=$THISLIB.lib\n" >"$THISNINJA"
    printf "
sdcc_lib_src_dir=$SDCC_LIB_SRC_LOC
dest_root = $JJ_LIB_DEST_ROOT

" >>"$THISNINJA"
printf '

lib = $dest_root/lib/$thislib

as_opts = -xlosff -g

builddir = build

peepopt = --peep-file $dest_root/include/jj_peep.def


cc_oc = -mz80n --std-c23 $
  --opt-code-size --reserve-regs-iy $
  --Werror --peep-return $
    $peepopt  $
  -I $dest_root/include $
  -I $dest_root/include -c -o


cc_opts = --max-allocs-per-node90000 $cc_oc

cco2 = --max-allocs-per-node25000 $cc_oc

cco3m = --max-allocs-per-node3000000 $cc_oc

rule cp
  command = cp $in $out

rule ar
  command = sdar r $out $in

rule as
  command = sdasz80 $as_opts $out $in

rule runlib
  command = sdrunlib $out

rule cc
  command = sdcc $cc_opts $out $in

rule cc2
  command = sdcc $cco2 $out $in

rule cc3m
  command = sdcc $cco3m $out $in

' >> "$THISNINJA"
}

generate_footer() {
    printf "\nbuild \$lib: ar $ALL_ASM\$\n$ALL_CC\n\n" >>"$THISNINJA"
}

generate_footercrt() {
    printf "\nbuild \$dest_root/lib/$THECRT0.rel: cp \$builddir/$THECRT0.rel\n\n" >>"$THISNINJA"
    printf "\nbuild all: phony \$lib \$dest_root/lib/$THECRT0.rel\n\n" >>"$THISNINJA"
    generate_footer
}

jjbegin() {
    rm -f "$JJ_LIB_DEST_ROOT/lib/$THISLIB.lib"
    [ -d build ] || mkdir build
    rm -f build/*
    generate_header
}

jjbegin_nodel() {
    generate_header
}


jjend() {
    generate_footer
    time ninja $NJOBS -v -f "$THISNINJA" || die
    echo "OK"
}


jjendcrt() {
    generate_footercrt
    time ninja $NJOBS -v -f "$THISNINJA" || die
    echo "OK"
}

