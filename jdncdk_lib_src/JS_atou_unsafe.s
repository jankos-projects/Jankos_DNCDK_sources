; part of Janko Stamenović's Janko's Dot NextZXOS C Development Kit library
;
;   The MIT License
;
; Copyright © 2024 Janko Stamenović
;
; Permission is hereby granted, free of charge, to any person
; obtaining a copy of this software and associated documentation
; files (the “Software”), to deal in the Software without
; restriction, including without limitation the rights to use, copy,
; modify, merge, publish, distribute, sublicense, and/or sell copies
; of the Software, and to permit persons to whom the Software is
; furnished to do so, subject to the following conditions:
;
; The above copyright notice and this permission notice shall be
; included in all copies or substantial portions of the Software.
;
; THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY
; KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
; WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE
; AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
; HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
; WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
; OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
; DEALINGS IN THE SOFTWARE.
;
        .module JS_atou_unsafe_
        .globl _JS_atou_unsafe
        .area _CODE


; unsigned JS_atou_unsafe( BYTE cb, BYTE* p )
_JS_atou_unsafe::
        ld b, a   ; b counter
        ld hl, #0 ; result

l1:     push  bc

        ld b, h
        ld c, l
        add hl, hl  ; hl *= 2
        add hl, hl  ; hl *= 2
        add hl, bc  ; hl is 5 times init
        add hl, hl  ; hl is 10 times init

        ld a, (de)
        inc de
        sub a, #'0'
        ld b, #0
        ld c, a
        add hl, bc

        pop bc
        djnz l1

        ex de, hl
        ret
