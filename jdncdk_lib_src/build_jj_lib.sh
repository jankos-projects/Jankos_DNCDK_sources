#!/bin/sh

THISLIB=jj_lib

#shellcheck source=./../common.sh.inc
. "$(dirname "$0")"/../common.sh.inc

jjbegin

# as

jjasm  efclose
jjasm  efopen
jjasm  efopen_h
jjasm  efread
jjasm  efwrite
jjasm  ef_fstat
jjasm  efseek

jjasm  JS_atou_unsafe

jjasm  print_hex_w
jjasm  print_sz
jjasm  print_sz_r

# cc
jjcc  JS_u8n_bytes_differ
jjcc  JS_fix_on_custom_error
jjcc  JS_atoul_safer
jjcc  JS_atou_safer
jjcc  JJS_ltoa_ulbuff
jjcc  JJS_num_print_buffer
jjcc  JS_get_ith_nibble
jjcc  JS_itoa
jjcc  JS_ltoa
jjcc  JS_nibble_to_hex
jjcc  JS_pitoa
jjcc  JS_pltoa
jjcc  print_int
jjcc  print_long
jjcc  print_plong
jjcc  print_pulong
jjcc  print_uint
jjcc  print_ulong
jjcc  JS_pultoa
jjcc  JS_putoa
jjcc  JS_p_toa16
jjcc  JS_ultoa
jjcc  JS_utoa

jjcc  printchar
jjcc  print_r
jjcc  putchar

jjend
