; part of Janko Stamenović's Janko's Dot NextZXOS C Development Kit library
;
; this file is released under the unlicense:
; unlicense.org (practically public domain)
;
; Note: the library as a whole is licensed differently
;
        .module print_sz_
        .globl _print_sz
        .area _CODE

;extern void print_sz( const char * sz )
; hl = sz txt address
_print_sz::
psz_loop:
        ld a, (hl)
        and a, a
        ret z
        rst #0x10
        inc hl
        jr psz_loop
