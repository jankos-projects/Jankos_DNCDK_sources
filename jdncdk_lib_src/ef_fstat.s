; part of Janko Stamenović's Janko's Dot NextZXOS C Development Kit library
;
; this file is released under the unlicense:
; unlicense.org (practically public domain)
;
; Note: the library as a whole is licensed differently
;
        .module ef_fstat_
        .globl _ef_fstat
        .area _CODE

;extern _Bool ef_fstat( unsigned char handle, struct esx_stat* buff );
_ef_fstat::
        ld  hl, #_JJ_errno
        ld (hl), #0
        ex de, hl ; buff from DE to  hl
        rst     #0x8  ; handle already in A
        .db     #0xa1
        jr nc, good
        ld (#_JJ_errno), a
good:
        ld a, #0
        rla
        xor a, #1
        ret
