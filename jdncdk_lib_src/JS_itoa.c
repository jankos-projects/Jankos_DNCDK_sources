#include <jj_lib.h>

// signed numbers formatting

char* JS_itoa( int i, char* s7 )
{
    if ( i < 0 ) {
        i = -i;
        *s7++ = '-';
    }
    return JS_utoa( i, s7 );
}

