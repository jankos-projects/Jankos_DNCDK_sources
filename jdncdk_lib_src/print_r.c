// part of Janko Stamenović's Janko's Dot NextZXOS C Development Kit library
//
// this file is released under the unlicense:
// unlicense.org (practically public domain)
//
// Note: the library as a whole is licensed differently
//
#include <jj_lib.h>

void print_r( void ) __preserves_regs( b, c, d, e, h, l )
{
    __asm
        ld a, #'\r'
        rst #0x10
        ld a,(#_JJ_crt_bits)
        bit 0, a   ; should also disable "scroll?"
        ret Z
        ld a, #0xff
        ld (#23692), a ; disable "scroll?" prompt
    __endasm;
}

