; part of Janko Stamenović's Janko's Dot NextZXOS C Development Kit library
;
; this file is released under the unlicense:
; unlicense.org (practically public domain)
;
; Note: the library as a whole is licensed differently
;
        .module efopen_h_
        .globl _efopen_h
        .area _CODE


;extern unsigned char efopen_h( unsigned char *fn, unsigned mode, void* header );
_efopen_h::
        ; filename is in HL already
        ; mode is passed in DE (16-bit to please __stdccall(1))
        pop     af
        pop     bc
        push    af
        ld      a, e ; mode
        ld      e, c ; header
        ld      d, b ; header
        ld      b, a ; mode
        xor     a
        ld      (#_JJ_errno), a  ; init: no error
        ld  a,  #'*'
        rst     #0x8
        .db     #0x9a
        ret     NC  ; on success return the handle already in A
        ld      (#_JJ_errno), a  ; save error code
        xor     a   ; return 0 on failure
        ret
