#include <jj_lib.h>

char JS_nibble_to_hex( char i )
{
    unsigned char c = i + '0';
    if ( c < '9'+1 )
        return c;
    c += 'a' - '0' - 10;
    return c;
}

