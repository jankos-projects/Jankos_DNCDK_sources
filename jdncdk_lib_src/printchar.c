// part of Janko Stamenović's Janko's Dot NextZXOS C Development Kit library
//
// this file is released under the unlicense:
// unlicense.org (practically public domain)
//
// Note: the library as a whole is licensed differently
//
#include "jj_lib.h"


void printchar( char c  ) __preserves_regs( b, c, d, e, h, l )
{
    __asm__ ( "rst #0x10 \n" );
    c;
}
