/* JS_utoa.c         part of Janko Stamenović's Janko's Dot NextZXOS C Development Kit library

    The MIT License

Copyright © 2024 Janko Stamenović

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the “Software”), to deal in the Software without
restriction, including without limitation the rights to use, copy,
modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY
KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE
AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
DEALINGS IN THE SOFTWARE.
*/
#include <jj_lib.h>

//
// unsigned numbers formatting

unsigned char JJS_toa10_bcd[5];

char* JS_bcd_to_str( char* s10 )
{
    unsigned char i = 0;
    unsigned char n;
    _Bool b_w = 0;
    do {
        n = JS_get_ith_nibble( (9-i), JJS_toa10_bcd );
        if ( n != 0 || i == 9 )
            b_w = 1;
        if ( b_w )
            *s10++ = n + (unsigned char)'0';
    } while ( ++i < 10 );
    *s10 = 0;
    return s10;
}

char* JS_utoa( unsigned v, char* s6 )
{
    __ultobcd( v, JJS_toa10_bcd );
    return JS_bcd_to_str( s6 );
}



