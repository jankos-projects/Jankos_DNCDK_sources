; part of Janko Stamenović's Janko's Dot NextZXOS C Development Kit library
;
; this file is released under the unlicense:
; unlicense.org (practically public domain)
;
; Note: the library as a whole is licensed differently
;
        .module efopen_
        .globl _efopen
        .area _CODE


; note unless mode is 16-bit it would need stack frame(!)
;extern unsigned char efopen( unsigned char *fn, unsigned mode );
_efopen::
        ; filename is in HL already
        ; mode is passed in DE (16-bit to please __stdccall(1))
        xor     a
        ld      (#_JJ_errno), a  ; init: no error
        ld  b,  e ; mode
        ld  a,  #'*'
        rst     #0x8
        .db     #0x9a
        ret     NC  ; on success return the handle already in A
        ld      (#_JJ_errno), a  ; save error code
        xor     a   ; return 0 on failure
        ret
