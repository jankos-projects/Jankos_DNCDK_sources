; part of Janko Stamenović's Janko's Dot NextZXOS C Development Kit library
;
; this file is released under the unlicense:
; unlicense.org (practically public domain)
;
; Note: the library as a whole is licensed differently
;
        .module efread_
        .globl _efread
        .area _CODE


;extern unsigned short efread(unsigned char handle, unsigned char* buf, unsigned short countb );
_efread::
        pop  hl     ; ret addr -: HL
        pop  bc     ; countb -: bc
        push hl     ; ret addr to the stack
        ld    hl,  #_JJ_errno
        ld  (hl), #0   ; reset status before read
        ex de, hl   ; buf -: HL
        rst   #0x8  ; handle still in A, buf in HL, countb in BC
        .db   #0x9d
        jr nc, good
        ld  (#_JJ_errno), a ; if not good: store status
        ld b, #0
        ld c, b
good:
        ld  d,b
        ld  e,c
        ret
