; part of Janko Stamenović's Janko's Dot NextZXOS C Development Kit library
;
; this file is released under the unlicense:
; unlicense.org (practically public domain)
;
; Note: the library as a whole is licensed differently
;
        .module print_sz_r_
        .globl _print_sz_r
        .area _CODE


; extern void print_sz_r(char * t)
; hl = sz txt address
_print_sz_r:
        call _print_sz
        jp _print_r ; also forces disable scroll
