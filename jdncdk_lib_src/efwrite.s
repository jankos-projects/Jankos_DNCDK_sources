; part of Janko Stamenović's Janko's Dot NextZXOS C Development Kit library
;
; this file is released under the unlicense:
; unlicense.org (practically public domain)
;
; Note: the library as a whole is licensed differently
;
        .module efwrite_
        .globl _efwrite
        .area _CODE

;extern unsigned fwrite(unsigned char handle, unsigned char* buf, unsigned short bytes);
_efwrite::
        ; third is the first on the stack
        pop  hl
        pop  bc
        push hl
        ; second is in de
        ex de, hl
        ; first is still  in a
        rst     #0x8
        .db     #0x9e
        ld d, b ; no known failure code here
        ld e, c ; just returns bytes "actually written"
        ret
