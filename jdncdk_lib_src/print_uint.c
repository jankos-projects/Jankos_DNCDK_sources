#include <jj_lib.h>

void print_uint( unsigned u )
{
    JS_utoa( u, JJS_num_print_buffer );
    print_sz( JJS_num_print_buffer );
}
