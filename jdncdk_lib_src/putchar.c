#include <jj_lib.h>

int putchar( int c ) {
    printchar( c == '\n'
                ? '\r'
                : c );
    return c;
}
