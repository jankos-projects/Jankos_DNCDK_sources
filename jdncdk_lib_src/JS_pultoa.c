/* JS_pultoa.c         part of Janko Stamenović's Janko's Dot NextZXOS C Development Kit library

    The MIT License

Copyright © 2024 Janko Stamenović

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the “Software”), to deal in the Software without
restriction, including without limitation the rights to use, copy,
modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY
KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE
AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
DEALINGS IN THE SOFTWARE.
*/
#include <jj_lib.h>

// unsigned 32-bit formatting

char* JS_pultoa( const unsigned long* v, char* s11 )
{
    // input is 5 bytes, 10 nibbles.
    // skip up to 9 nibbles if they are 0
    // on first non zero, or 9th copy each nibble as ASC
    __ultobcd( *v, JJS_toa10_bcd );
    return JS_bcd_to_str( s11 );
}

