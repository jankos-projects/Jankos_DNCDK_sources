#include <jj_lib.h>

// returns some nonzero when differ
unsigned char JS_u8n_bytes_differ( unsigned char n, unsigned char* p, unsigned char* q  ) __naked {
    __asm;
    pop bc
    pop hl
    push bc
    ld b, a
l1:
    ld a, (de)
    sub a, (hl)
    ret nz
    inc de
    inc hl
    djnz l1
    ret
    __endasm;
    n, p, q;
}
