        .module efseek_
        .globl _efseek
        .area _CODE

; mode and handle have to be combined in one value,
; mode in the upper byte
; luckily esx_seek_set is 0 so that is relevant only for
;        add or subtract modes
; returns 1 on success, 0 on failure.
; error code is in the JJ_status
; note: it updates the pos to the current after the seek!
;extern unsigned char efseek( unsigned int modehi_handlelo, unsigned long* pos );
_efseek::
        ; handle in L mode in H
        ; DE points to the pos
        push    de
        push    hl
        ex      de, hl
        ld      e, (hl)
        inc     hl
        ld      d, (hl)
        inc     hl
        ld      c, (hl)
        inc     hl
        ld      b, (hl)
        pop     hl
        xor     a
        ld      (#_JJ_errno), a
        ld      a, l  ; handle
        ld      l, h  ; mode
        ; expects: mode in L (when dot commad)
        ; handle in in A
        ; BCDE as pos
        rst     #0x8
        .db     #0x9f
        jr      NC, goodseek
        pop     hl
        ld      (#_JJ_errno), a  ; save error code
        xor     a   ; return 0 on failure
        ret
goodseek:
        pop     hl
        ld      (hl),e
        inc     hl
        ld      (hl),d
        inc     hl
        ld      (hl),c
        inc     hl
        ld      (hl),b
        ld      a, #1
        ret

