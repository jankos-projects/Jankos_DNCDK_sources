; part of Janko Stamenović's Janko's Dot NextZXOS C Development Kit library
;
; this file is released under the unlicense:
; unlicense.org (practically public domain)
;
; Note: the library as a whole is licensed differently
;
        .zxn
        .module print_hex_w_
        .globl _print_hex_w
        .globl _print_hex_byte

        .area _CODE

; extern void print_hex_w( unsigned w );
_print_hex_w::
        ld a, h
        call _print_hex_byte
        ld a, l
        ; fall through to _print_hex_byte
; extern void print_hex_byte( unsigned char byte );
_print_hex_byte::
        ld     c, a
        swapnib
        call    _print_hex_nibble        ; upper
        ld      a, c
        ; fall through to print_nibble for lower
_print_hex_nibble:
        and     #0x0F
        cp      #10
        jr      nc, print_hn_a_to_f
        add     a,#'0'
        rst     #0x10
        ret
print_hn_a_to_f:
        add     a,#('a'-10)
        rst     #0x10
        ret
