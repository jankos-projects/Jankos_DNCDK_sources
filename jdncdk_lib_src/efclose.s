; part of Janko Stamenović's Janko's Dot NextZXOS C Development Kit library
;
; this file is released under the unlicense:
; unlicense.org (practically public domain)
;
; Note: the library as a whole is licensed differently
;
        .module efclose_
        .globl _efclose
        .area _CODE


;extern void efclose(unsigned char handle);
_efclose::
        rst     #0x8  ; handle already in A
        .db     #0x9b
        ret
