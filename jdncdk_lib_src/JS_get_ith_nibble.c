#include <jj_lib.h>


unsigned char JS_get_ith_nibble( unsigned char i, void* bytes )
{
    unsigned char c = ((unsigned char*)bytes)[ i>>1 ];
    if ( i & 1 ) c >>= 4;
    return 0xf & c;
}
