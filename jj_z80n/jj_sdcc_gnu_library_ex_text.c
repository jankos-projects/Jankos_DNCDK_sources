char* jj_sdcc_gnu_lincense_2_ex_text ="\n"
"IMPORTANT! The special exception quoted below is valid ONLY\n"
"for the specific source files in which it appears, it CAN\n"
"NOT be applied for the other GNU GENERAL PUBLIC licensed\n"
"material in this package:\n"
"\n"
"\n"
"This library is free software; you can redistribute it and/or\n"
"modify it under the terms of the GNU General Public License as\n"
"published by the Free Software Foundation; either version 2, or\n"
"(at your option) any later version.\n"
"\n"
"This library is distributed in the hope that it will be useful,\n"
"but WITHOUT ANY WARRANTY; without even the implied warranty of\n"
"MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU\n"
"General Public License for more details.\n"
"\n"
"You should have received a copy of the GNU General Public License\n"
"along with this library; see the file COPYING. If not, write to\n"
"the Free Software Foundation, 51 Franklin Street, Fifth Floor,\n"
"Boston, MA 02110-1301, USA.\n"
"\n"
"As a special exception, if you link this library with other files,\n"
"some of which are compiled with SDCC, to produce an executable,\n"
"this library does not by itself cause the resulting executable to\n"
"be covered by the GNU General Public License. This exception does\n"
"not however invalidate any other reasons why the executable file\n"
"might be covered by the GNU General Public License.\n"
;
