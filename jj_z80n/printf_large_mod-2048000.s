;--------------------------------------------------------
; File Created by SDCC : free open source ISO C Compiler
; Version 4.4.1 #14899 (MINGW64)
;--------------------------------------------------------
	.zxn
	.module printf_large_mod
	.optsdcc -mz80n sdcccall(1)

;--------------------------------------------------------
; Public variables in this module
;--------------------------------------------------------
	.globl _strlen
	.globl __print_format
;--------------------------------------------------------
;/*-----------------------------------------------------------------
;   printf_large.c - formatted output conversion
;
;   Copyright (C) 1999, Martijn van Balen <aed AT iae.nl>
;   Added %f By - <johan.knol AT iduna.nl> (2000)
;   Refactored by - Maarten Brock (2004)
;
;  printf_large_mod.c
;   (C) 2024:  Janko Stamenović modifications for
;                Janko's Dot NextZXOS C Development Kit library
;
;   This library is free software; you can redistribute it and/or modify it
;   under the terms of the GNU General Public License as published by the
;   Free Software Foundation; either version 2, or (at your option) any
;   later version.
;
;   This library is distributed in the hope that it will be useful,
;   but WITHOUT ANY WARRANTY; without even the implied warranty of
;   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;   GNU General Public License for more details.
;
;   You should have received a copy of the GNU General Public License
;   along with this library; see the file COPYING. If not, write to the
;   Free Software Foundation, 51 Franklin Street, Fifth Floor, Boston,
;   MA 02110-1301, USA.
;
;   As a special exception, if you link this library with other files,
;   some of which are compiled with SDCC, to produce an executable,
;   this library does not by itself cause the resulting executable to
;   be covered by the GNU General Public License. This exception does
;   not however invalidate any other reasons why the executable file
;   might be covered by the GNU General Public License.
;-------------------------------------------------------------------------*/
;
;--------------------------------------------------------
; special function registers
;--------------------------------------------------------
;--------------------------------------------------------
; ram data
;--------------------------------------------------------
	.area _DATA
_lower_case:
	.ds 1
_output_char:
	.ds 2
_p:
	.ds 2
_value:
	.ds 5
_charsOutputted:
	.ds 2
;--------------------------------------------------------
; ram data
;--------------------------------------------------------
	.area _INITIALIZED
;--------------------------------------------------------
; absolute external ram data
;--------------------------------------------------------
	.area _DABS (ABS)
;--------------------------------------------------------
; global & static initialisations
;--------------------------------------------------------
	.area _HOME
	.area _GSINIT
	.area _GSFINAL
	.area _GSINIT
;--------------------------------------------------------
; Home
;--------------------------------------------------------
	.area _HOME
	.area _HOME
;--------------------------------------------------------
; code
;--------------------------------------------------------
	.area _CODE
;printf_large_mod.c:88: _output_char (unsigned char c)
;	---------------------------------
; Function _output_char
; ---------------------------------
__output_char:
;printf_large_mod.c:90: output_char( c, p );
	ld	de, (_p)
	ld	hl, (_output_char)
	call	___sdcc_call_hl
;printf_large_mod.c:91: charsOutputted++;
	ld	hl, (_charsOutputted)
	inc	hl
	ld	(_charsOutputted), hl
;printf_large_mod.c:92: }
	ret
;printf_large_mod.c:97: output_digit (unsigned char n)
;	---------------------------------
; Function output_digit
; ---------------------------------
_output_digit:
;printf_large_mod.c:99: register unsigned char c = n + (unsigned char)'0';
	add	a, #0x30
	ld	c, a
;printf_large_mod.c:101: if (c > (unsigned char)'9')
	ld	a, #0x39
	sub	a, c
	jr	NC, 00104$
;printf_large_mod.c:103: c += (unsigned char)('A' - '0' - 10);
	ld	a, c
	add	a, #0x07
	ld	c, a
;printf_large_mod.c:104: if (lower_case)
	ld	hl, #_lower_case
	bit	0, (hl)
	jr	Z, 00104$
;printf_large_mod.c:105: c = tolower(c);
	set	5, c
00104$:
;printf_large_mod.c:107: _output_char( c );
	ld	a, c
;printf_large_mod.c:108: }
	jp	__output_char
;printf_large_mod.c:114: output_2digits (unsigned char b)
;	---------------------------------
; Function output_2digits
; ---------------------------------
_output_2digits:
;printf_large_mod.c:116: output_digit( b>>4   );
	ld	c, a
	swap	a
	and	a, #0x0f
	push	bc
	call	_output_digit
	pop	bc
;printf_large_mod.c:117: output_digit( b&0x0F );
	ld	a, c
	and	a, #0x0f
;printf_large_mod.c:118: }
	jp	_output_digit
;printf_large_mod.c:123: calculate_digit (unsigned char radix)
;	---------------------------------
; Function calculate_digit
; ---------------------------------
_calculate_digit:
	call	___sdcc_enter_ix
	push	af
	ld	-2 (ix), a
;printf_large_mod.c:125: register unsigned long ul = value.ul;
	ld	de, (#_value + 0)
	ld	bc, (#_value + 2)
;printf_large_mod.c:126: register unsigned char b4 = value.byte[4];
	ld	a, (#(_value + 4) + 0)
	ld	-1 (ix), a
;printf_large_mod.c:129: do
	ld	l, #0x20
00103$:
;printf_large_mod.c:131: b4 = (b4 << 1);
	ld	a, -1 (ix)
	add	a, a
	ld	h, a
;printf_large_mod.c:132: b4 |= (ul >> 31) & 0x01;
	ld	a, b
	rlca
	and	a, #0x01
	ld	-1 (ix), a
	or	a, h
	ld	-1 (ix), a
;printf_large_mod.c:133: ul <<= 1;
	ex	de, hl
	add	hl, hl
	ex	de, hl
	rl	c
	rl	b
;printf_large_mod.c:135: if (radix <= b4 )
	ld	a, -1 (ix)
	sub	a, -2 (ix)
	jr	C, 00104$
;printf_large_mod.c:137: b4 -= radix;
	ld	a, -1 (ix)
	sub	a, -2 (ix)
	ld	-1 (ix), a
;printf_large_mod.c:138: ul |= 1;
	set	0, e
00104$:
;printf_large_mod.c:140: } while (--i);
	dec	l
	jr	NZ, 00103$
;printf_large_mod.c:141: value.ul = ul;
	ld	(_value), de
	ld	(_value+2), bc
;printf_large_mod.c:142: value.byte[4] = b4;
	ld	hl, #(_value + 4)
	ld	a, -1 (ix)
	ld	(hl), a
;printf_large_mod.c:143: }
	pop	af
	pop	ix
	ret
;printf_large_mod.c:164: _print_format (pfn_outputchar pfn, void* pvoid, const char *format, va_list ap)
;	---------------------------------
; Function _print_format
; ---------------------------------
__print_format::
	push	ix
	ld	ix,#0
	add	ix,sp
	ld	c, l
	ld	b, h
	ld	hl, #-14
	add	hl, sp
	ld	sp, hl
	ld	l, c
	ld	h, b
	ld	(_output_char), hl
	ld	hl, #_p
	ld	(hl), e
	inc	hl
	ld	(hl), d
;printf_large_mod.c:179: charsOutputted = 0;
	xor	a, a
	ld	hl, #_charsOutputted
	ld	(hl), a
	inc	hl
	ld	(hl), a
;printf_large_mod.c:181: while( c=*format++ )
00231$:
	ld	l, 4 (ix)
	ld	h, 5 (ix)
	ld	c, (hl)
	inc	hl
	ld	4 (ix), l
	ld	5 (ix), h
	ld	a, c
	inc	c
	dec	c
	jp	Z, 00233$
;printf_large_mod.c:183: if ( c=='%' )
	cp	a, #0x25
	jp	NZ,00229$
;printf_large_mod.c:185: flags_a = 0;
	ld	c, #0x00
;printf_large_mod.c:187: radix           = 0;
	ld	-8 (ix), #0x00
;printf_large_mod.c:188: width           = 0;
	ld	-7 (ix), #0x00
;printf_large_mod.c:189: decimals        = DECIMALS_NONE;
	ld	-6 (ix), #0xff
;printf_large_mod.c:191: get_conversion_spec:
	ld	a, 4 (ix)
	ld	-2 (ix), a
	ld	a, 5 (ix)
	ld	-1 (ix), a
00101$:
;printf_large_mod.c:193: c = *format++;
	ld	l, -2 (ix)
	ld	h, -1 (ix)
	ld	b, (hl)
	inc	-2 (ix)
	jr	NZ, 00783$
	inc	-1 (ix)
00783$:
	ld	a, -2 (ix)
	ld	4 (ix), a
	ld	a, -1 (ix)
	ld	5 (ix), a
;printf_large_mod.c:195: if (c=='%')
;printf_large_mod.c:197: OUTPUT_CHAR(c, p);
	ld	a,b
	cp	a,#0x25
	jr	NZ, 00103$
	call	__output_char
;printf_large_mod.c:198: continue;
	jr	00231$
00103$:
;printf_large_mod.c:203: if (decimals==DECIMALS_NONE)
	ld	a, -6 (ix)
	inc	a
	ld	a, #0x01
	jr	Z, 00787$
	xor	a, a
00787$:
	ld	-5 (ix), a
;printf_large_mod.c:201: if (isdigit(c))
	ld	a, b
	sub	a, #0x30
	jr	C, 00110$
	ld	a, #0x39
	sub	a, b
	jr	C, 00110$
;printf_large_mod.c:205: width = 10*width + c - '0';
;printf_large_mod.c:203: if (decimals==DECIMALS_NONE)
	ld	a, -5 (ix)
	or	a, a
	jr	Z, 00107$
;printf_large_mod.c:205: width = 10*width + c - '0';
	ld	a, -7 (ix)
	ld	e, a
	add	a, a
	add	a, a
	add	a, e
	add	a, a
	add	a, b
	add	a, #0xd0
;printf_large_mod.c:206: if (width == 0)
	ld	-7 (ix), a
	or	a, a
	jr	NZ, 00101$
;printf_large_mod.c:209: SET_FLAGSA( FLAGSA_ZERO_PADDING );
	set	1, c
	jr	00101$
00107$:
;printf_large_mod.c:214: decimals = 10*decimals + c - '0';
	ld	a, -6 (ix)
	ld	e, a
	add	a, a
	add	a, a
	add	a, e
	add	a, a
	add	a, b
	add	a, #0xd0
	ld	-6 (ix), a
;printf_large_mod.c:216: goto get_conversion_spec;
	jr	00101$
00110$:
;printf_large_mod.c:219: if (c=='.')
	ld	a, b
	sub	a, #0x2e
	jr	NZ, 00115$
;printf_large_mod.c:221: if (decimals==DECIMALS_NONE)
	ld	a, -5 (ix)
	or	a, a
	jr	Z, 00101$
;printf_large_mod.c:222: decimals=0;
	ld	-6 (ix), #0x00
;printf_large_mod.c:225: goto get_conversion_spec;
	jr	00101$
00115$:
;printf_large_mod.c:228: if (islower(c))
	ld	a, b
	sub	a, #0x61
	jr	C, 00117$
	ld	a, #0x7a
	sub	a, b
	jr	C, 00117$
;printf_large_mod.c:230: c = toupper(c);
	res	5, b
;printf_large_mod.c:231: lower_case = 1;
	ld	hl, #_lower_case
	ld	(hl), #0x01
	jr	00118$
00117$:
;printf_large_mod.c:234: lower_case = 0;
	xor	a, a
	ld	(#_lower_case),a
00118$:
;printf_large_mod.c:236: switch( c )
	ld	a,b
	cp	a,#0x20
	jr	Z, 00122$
	cp	a,#0x2b
	jr	Z, 00121$
	cp	a,#0x2d
	jr	Z, 00120$
	sub	a, #0x42
	jr	Z, 00123$
;printf_large_mod.c:264: c = va_arg(ap,int);
	ld	a, 6 (ix)
	add	a, #0x02
	ld	-4 (ix), a
	ld	a, 7 (ix)
	adc	a, #0x00
	ld	-3 (ix), a
	ld	e, -4 (ix)
	ld	d, -3 (ix)
	dec	de
	dec	de
;printf_large_mod.c:236: switch( c )
	ld	a,b
	cp	a,#0x43
	jr	Z, 00129$
	sub	a, #0x44
	jp	Z,00154$
	ld	a,b
	cp	a,#0x46
	jp	Z,00158$
	cp	a,#0x48
	jp	Z,00101$
	sub	a, #0x49
	jp	Z,00154$
	ld	a,b
	cp	a,#0x4a
	jp	Z,00101$
;printf_large_mod.c:269: PTR = va_arg(ap,ptr_t);
;printf_large_mod.c:236: switch( c )
	cp	a,#0x4c
	jr	Z, 00128$
	cp	a,#0x4f
	jp	Z,00155$
	cp	a,#0x50
	jp	Z,00152$
	cp	a,#0x53
	jr	Z, 00133$
	cp	a,#0x54
	jp	Z,00101$
	sub	a, #0x55
	jp	Z,00156$
	ld	a,b
	cp	a,#0x58
	jp	Z,00157$
	sub	a, #0x5a
	jp	Z,00101$
	jp	00159$
;printf_large_mod.c:238: case '-':
00120$:
;printf_large_mod.c:239: SET_FLAGSA( FLAGSA_LEFT_JUSTIFY );
	set	0, c
;printf_large_mod.c:240: goto get_conversion_spec;
	jp	00101$
;printf_large_mod.c:241: case '+':
00121$:
;printf_large_mod.c:242: SET_FLAGSA( FLAGSA_PREFIX_SIGN );
	set	2, c
;printf_large_mod.c:243: goto get_conversion_spec;
	jp	00101$
;printf_large_mod.c:244: case ' ':
00122$:
;printf_large_mod.c:245: SET_FLAGSA( FLAGSA_PREFIX_SPACE );
	set	3, c
;printf_large_mod.c:246: goto get_conversion_spec;
	jp	00101$
;printf_large_mod.c:247: case 'B': /* byte */
00123$:
;printf_large_mod.c:248: SET_FLAGSA( FLAGSA_CHAR_ARGUMENT );
	set	5, c
;printf_large_mod.c:249: goto get_conversion_spec;
	jp	00101$
;printf_large_mod.c:256: case 'L': /* long */
00128$:
;printf_large_mod.c:257: SET_FLAGSA( FLAGSA_LONG_ARGUMENT );
	set	6, c
;printf_large_mod.c:258: goto get_conversion_spec;
	jp	00101$
;printf_large_mod.c:260: case 'C':
00129$:
;printf_large_mod.c:261: if ( IS_FLAGSA(FLAGSA_CHAR_ARGUMENT) )
	bit	5, c
	jr	Z, 00131$
;printf_large_mod.c:262: c = va_arg(ap,char);
	ld	l, 6 (ix)
	ld	h, 7 (ix)
	inc	hl
	ld	6 (ix), l
	ld	7 (ix), h
	dec	hl
	ld	d, (hl)
	jr	00132$
00131$:
;printf_large_mod.c:264: c = va_arg(ap,int);
	ld	a, -4 (ix)
	ld	6 (ix), a
	ld	a, -3 (ix)
	ld	7 (ix), a
	ld	a, (de)
	ld	d, a
00132$:
;printf_large_mod.c:265: OUTPUT_CHAR( c, p );
	push	bc
	ld	a, d
	call	__output_char
	pop	bc
;printf_large_mod.c:266: break;
	jp	00160$
;printf_large_mod.c:268: case 'S':
00133$:
;printf_large_mod.c:269: PTR = va_arg(ap,ptr_t);
	ld	a, -4 (ix)
	ld	6 (ix), a
	ld	a, -3 (ix)
	ld	7 (ix), a
	ld	l, e
	ld	h, d
	ld	a, (hl)
	inc	hl
	ld	h, (hl)
	ld	l, a
	ld	(_value), hl
;printf_large_mod.c:271: length = strlen(PTR);
	push	bc
	call	_strlen
	pop	bc
;printf_large_mod.c:272: if ( decimals == DECIMALS_NONE )
	ld	a, -5 (ix)
	or	a, a
	jr	Z, 00135$
;printf_large_mod.c:274: decimals = length;
	ld	-6 (ix), e
00135$:
;printf_large_mod.c:276: if ( ( !IS_FLAGSA(FLAGSA_LEFT_JUSTIFY) ) && (length < width) )
	ld	a, c
	and	a, #0x01
	ld	d, a
	or	a, a
	jr	NZ, 00277$
	ld	a, e
	sub	a, -7 (ix)
	jr	NC, 00277$
;printf_large_mod.c:278: width -= length;
	ld	a, -7 (ix)
	sub	a, e
	ld	b, a
;printf_large_mod.c:279: while( width-- != 0 )
00136$:
	ld	a, b
	dec	b
	or	a, a
	jr	Z, 00312$
;printf_large_mod.c:281: OUTPUT_CHAR( ' ', p );
	push	bc
	push	de
	ld	a, #0x20
	call	__output_char
	pop	de
	pop	bc
	jr	00136$
;printf_large_mod.c:285: while ( (c = *PTR)  && (decimals-- > 0))
00312$:
	ld	-7 (ix), b
00277$:
	ld	b, -6 (ix)
00143$:
	ld	hl, (#_value + 0)
	ld	a, (hl)
	ld	l, a
	or	a, a
	jr	Z, 00145$
	ld	a, b
	dec	b
	or	a, a
	jr	Z, 00145$
;printf_large_mod.c:287: OUTPUT_CHAR( c, p );
	push	bc
	push	de
	ld	a, l
	call	__output_char
	pop	de
	pop	bc
;printf_large_mod.c:288: PTR++;
	ld	hl, (#_value + 0)
	inc	hl
	ld	(_value), hl
	jr	00143$
00145$:
;printf_large_mod.c:291: if ( IS_FLAGSA(FLAGSA_LEFT_JUSTIFY) && (length < width))
	ld	a, d
	or	a, a
	jr	Z, 00160$
	ld	a, e
	sub	a, -7 (ix)
	jr	NC, 00160$
;printf_large_mod.c:293: width -= length;
	ld	a, -7 (ix)
	sub	a, e
	ld	b, a
;printf_large_mod.c:294: while( width-- != 0 )
00146$:
	ld	a, b
	dec	b
	or	a, a
	jr	Z, 00314$
;printf_large_mod.c:296: OUTPUT_CHAR( ' ', p );
	push	bc
	ld	a, #0x20
	call	__output_char
	pop	bc
	jr	00146$
;printf_large_mod.c:301: case 'P':
00152$:
;printf_large_mod.c:302: PTR = va_arg(ap,ptr_t);
	ld	a, -4 (ix)
	ld	6 (ix), a
	ld	a, -3 (ix)
	ld	7 (ix), a
	ex	de,hl
	ld	e, (hl)
	inc	hl
	ld	d, (hl)
	ld	(_value), de
;printf_large_mod.c:310: OUTPUT_CHAR('0', p);
	push	bc
	ld	a, #0x30
	call	__output_char
	ld	a, #0x78
	call	__output_char
;printf_large_mod.c:312: OUTPUT_2DIGITS( value.byte[1] );
	ld	a, (#(_value + 1) + 0)
	call	_output_2digits
;printf_large_mod.c:313: OUTPUT_2DIGITS( value.byte[0] );
	ld	a, (#_value + 0)
	call	_output_2digits
	pop	bc
;printf_large_mod.c:315: break;
	jr	00160$
;printf_large_mod.c:318: case 'I':
00154$:
;printf_large_mod.c:319: SET_FLAGSA( FLAGSA_SIGNED_ARGUMENT );
	set	4, c
;printf_large_mod.c:320: radix = 10;
	ld	-8 (ix), #0x0a
;printf_large_mod.c:321: break;
	jr	00160$
;printf_large_mod.c:323: case 'O':
00155$:
;printf_large_mod.c:324: radix = 8;
	ld	-8 (ix), #0x08
;printf_large_mod.c:325: break;
	jr	00160$
;printf_large_mod.c:327: case 'U':
00156$:
;printf_large_mod.c:328: radix = 10;
	ld	-8 (ix), #0x0a
;printf_large_mod.c:329: break;
	jr	00160$
;printf_large_mod.c:331: case 'X':
00157$:
;printf_large_mod.c:332: radix = 16;
	ld	-8 (ix), #0x10
;printf_large_mod.c:333: break;
	jr	00160$
;printf_large_mod.c:335: case 'F':
00158$:
;printf_large_mod.c:336: SET_FLAGSA( FLAGSA_FLOAT_ARGUMENT );
	set	7, c
;printf_large_mod.c:337: break;
	jr	00160$
;printf_large_mod.c:339: default:
00159$:
;printf_large_mod.c:341: OUTPUT_CHAR( c, p );
	push	bc
	ld	a, b
	call	__output_char
	pop	bc
;printf_large_mod.c:504: return charsOutputted;
	jr	00160$
;printf_large_mod.c:343: }
00314$:
	ld	-7 (ix), b
00160$:
;printf_large_mod.c:347: value.f = va_arg(ap, float);
	ld	a, 6 (ix)
	add	a, #0x04
	ld	b, a
	ld	a, 7 (ix)
	adc	a, #0x00
	ld	e, a
	ld	a, b
	add	a, #0xfc
	ld	-3 (ix), a
	ld	a, e
	adc	a, #0xff
	ld	-2 (ix), a
;printf_large_mod.c:345: if ( IS_FLAGSA( FLAGSA_FLOAT_ARGUMENT ) )
	bit	7, c
	jr	Z, 00226$
;printf_large_mod.c:347: value.f = va_arg(ap, float);
	ld	6 (ix), b
	ld	7 (ix), e
	ld	l, -3 (ix)
	ld	h, -2 (ix)
	ld	c, (hl)
	inc	hl
	ld	b, (hl)
	inc	hl
	ld	e, (hl)
	inc	hl
	ld	d, (hl)
	ld	(_value), bc
	ld	(_value+2), de
;printf_large_mod.c:348: PTR="<NO FLOAT>";
	ld	hl, #___str_0
	ld	(_value), hl
;printf_large_mod.c:349: while (c=*PTR++)
00161$:
	ld	hl, (#_value + 0)
	ld	c, l
	ld	b, h
	inc	bc
	ld	(_value), bc
	ld	a, (hl)
;printf_large_mod.c:351: OUTPUT_CHAR (c, p);
	or	a,a
	jp	Z,00231$
	call	__output_char
	jr	00161$
00226$:
;printf_large_mod.c:359: else if (radix != 0)
	ld	a, -8 (ix)
	or	a, a
	jp	Z, 00231$
;printf_large_mod.c:364: unsigned char MEM_SPACE_BUF_PP *pstore = &store[5];
;printf_large_mod.c:370: if (!!IS_FLAGSA( FLAGSA_SIGNED_ARGUMENT ) )
	ld	a, c
	and	a, #0x10
	ld	-1 (ix), a
;printf_large_mod.c:367: if (IS_FLAGSA( FLAGSA_CHAR_ARGUMENT ) )
	bit	5, c
	jr	Z, 00172$
;printf_large_mod.c:369: value.l = va_arg(ap, char);
	ld	l, 6 (ix)
	ld	h, 7 (ix)
	inc	hl
	ld	6 (ix), l
	ld	7 (ix), h
	dec	hl
	ld	e, (hl)
	ld	d, #0x00
	ld	hl, #0x0000
	ld	(_value), de
	ld	(_value+2), hl
;printf_large_mod.c:370: if (!!IS_FLAGSA( FLAGSA_SIGNED_ARGUMENT ) )
	ld	a, -1 (ix)
	or	a, a
	jr	Z, 00173$
;printf_large_mod.c:372: value.l &= 0xFF;
	ld	de, (#_value + 0)
	ld	hl, (#_value + 2)
	ld	d, #0x00
	ld	(_value), de
	ld	(_value+2), hl
	jr	00173$
00172$:
;printf_large_mod.c:375: else if (IS_FLAGSA( FLAGSA_LONG_ARGUMENT ))
	bit	6, c
	jr	Z, 00169$
;printf_large_mod.c:377: value.l = va_arg(ap, long);
	ld	6 (ix), b
	ld	7 (ix), e
	ld	l, -3 (ix)
	ld	h, -2 (ix)
	ld	e, (hl)
	inc	hl
	ld	d, (hl)
	inc	hl
	inc	hl
	ld	a, (hl)
	dec	hl
	ld	l, (hl)
	ld	h, a
	ld	(_value), de
	ld	(_value+2), hl
	jr	00173$
00169$:
;printf_large_mod.c:381: value.l = va_arg(ap, int);
	ld	l, 6 (ix)
	ld	h, 7 (ix)
	inc	hl
	inc	hl
	ld	6 (ix), l
	ld	7 (ix), h
	dec	hl
	dec	hl
	ld	e, (hl)
	inc	hl
	ld	a, (hl)
	ld	d, a
	rlca
	sbc	hl, hl
	ld	(_value), de
	ld	(_value+2), hl
;printf_large_mod.c:382: if (!IS_FLAGSA( FLAGSA_SIGNED_ARGUMENT ) )
	ld	a, -1 (ix)
	or	a, a
	jr	NZ, 00173$
;printf_large_mod.c:384: value.l &= 0xFFFF;
	ld	de, (#_value + 0)
	ld	hl, (#_value + 2)
	ld	hl, #0x0000
	ld	(_value), de
	ld	(_value+2), hl
00173$:
;printf_large_mod.c:388: if ( IS_FLAGSA( FLAGSA_SIGNED_ARGUMENT ) )
	ld	a, -1 (ix)
	or	a, a
	jr	Z, 00178$
;printf_large_mod.c:390: if (value.l < 0)
	ld	de, (#_value + 0)
	ld	hl, (#_value + 2)
	bit	7, h
	jr	Z, 00175$
;printf_large_mod.c:391: value.l = -value.l;
	ld	de, (#_value + 0)
	ld	hl, (#_value + 2)
	xor	a, a
	sub	a, e
	ld	e, a
	ld	a, #0x00
	sbc	a, d
	ld	d, a
	ld	a, #0x00
	sbc	a, l
	ld	l, a
	sbc	a, a
	sub	a, h
	ld	h, a
	ld	(_value), de
	ld	(_value+2), hl
	jr	00178$
00175$:
;printf_large_mod.c:393: CLEAR_FLAGSA( FLAGSA_SIGNED_ARGUMENT );
	res	4, c
00178$:
;printf_large_mod.c:397: lsd = 1;
	ld	-1 (ix), #0x01
;printf_large_mod.c:399: do {
	ld	hl, #5
	add	hl, sp
	ex	de, hl
	ld	b, #0x00
00182$:
;printf_large_mod.c:400: value.byte[4] = 0;
	ld	hl, #(_value + 4)
	ld	(hl), #0x00
;printf_large_mod.c:401: calculate_digit(radix);
	push	bc
	push	de
	ld	a, -8 (ix)
	call	_calculate_digit
	pop	de
	pop	bc
;printf_large_mod.c:404: *pstore = (value.byte[4] << 4) | (value.byte[4] >> 4) | *pstore;
	ld	hl, #(_value + 4)
	ld	l, (hl)
;printf_large_mod.c:402: if (!lsd)
	bit	0, -1 (ix)
	jr	NZ, 00180$
;printf_large_mod.c:404: *pstore = (value.byte[4] << 4) | (value.byte[4] >> 4) | *pstore;
	ld	a, l
	add	a, a
	add	a, a
	add	a, a
	add	a, a
	ld	-2 (ix), a
	ld	a, (#(_value + 4) + 0)
	swap	a
	and	a, #0x0f
	or	a, -2 (ix)
	ld	l, a
	ld	a, (de)
	or	a, l
	ld	(de), a
;printf_large_mod.c:405: pstore--;
	dec	de
	jr	00181$
00180$:
;printf_large_mod.c:409: *pstore = value.byte[4];
	ld	a, l
	ld	(de), a
00181$:
;printf_large_mod.c:411: length++;
	inc	b
;printf_large_mod.c:412: lsd = !lsd;
	ld	a, -1 (ix)
	xor	a, #0x01
	ld	-1 (ix), a
;printf_large_mod.c:413: } while( value.ul );
	push	de
	push	bc
	ld	de, #_value
	ld	hl, #13
	add	hl, sp
	ex	de, hl
	ld	bc, #0x0004
	ldir
	pop	bc
	pop	de
	ld	a, -2 (ix)
	or	a, -3 (ix)
	or	a, -4 (ix)
	or	a, -5 (ix)
	jr	NZ, 00182$
;printf_large_mod.c:415: if (width == 0)
	ld	-4 (ix), e
	ld	-3 (ix), d
	ld	a, -7 (ix)
	or	a, a
	jr	NZ, 00186$
;printf_large_mod.c:420: width = 1;
	ld	-7 (ix), #0x01
00186$:
;printf_large_mod.c:276: if ( ( !IS_FLAGSA(FLAGSA_LEFT_JUSTIFY) ) && (length < width) )
	ld	a, c
	and	a, #0x01
	ld	e, a
;printf_large_mod.c:424: if (!IS_FLAGSA( FLAGSA_ZERO_PADDING ) && !IS_FLAGSA( FLAGSA_LEFT_JUSTIFY ))
	bit	1, c
	jr	NZ, 00191$
	ld	a, e
	or	a, a
	jr	NZ, 00191$
;printf_large_mod.c:426: while ( width > (unsigned char) (length+1) )
	ld	d, -7 (ix)
00187$:
	ld	l, b
	inc	l
	ld	a, l
	sub	a, d
	jr	NC, 00316$
;printf_large_mod.c:428: OUTPUT_CHAR( ' ', p );
	push	bc
	push	de
	ld	a, #0x20
	call	__output_char
	pop	de
	pop	bc
;printf_large_mod.c:429: width--;
	dec	d
	jr	00187$
00316$:
	ld	-7 (ix), d
00191$:
;printf_large_mod.c:437: width--;
	ld	d, -7 (ix)
	dec	d
;printf_large_mod.c:433: if (IS_FLAGSA( FLAGSA_SIGNED_ARGUMENT )) // this now means the original value was negative
	bit	4, c
	jr	Z, 00201$
;printf_large_mod.c:435: OUTPUT_CHAR( '-', p );
	push	bc
	push	de
	ld	a, #0x2d
	call	__output_char
	pop	de
	pop	bc
;printf_large_mod.c:437: width--;
	ld	-7 (ix), d
	jr	00202$
00201$:
;printf_large_mod.c:439: else if (length != 0)
	ld	a, b
	or	a, a
	jr	Z, 00202$
;printf_large_mod.c:442: if (IS_FLAGSA( FLAGSA_PREFIX_SIGN ))
	bit	2, c
	jr	Z, 00196$
;printf_large_mod.c:444: OUTPUT_CHAR( '+', p );
	push	bc
	push	de
	ld	a, #0x2b
	call	__output_char
	pop	de
	pop	bc
;printf_large_mod.c:446: width--;
	ld	-7 (ix), d
	jr	00202$
00196$:
;printf_large_mod.c:448: else if (IS_FLAGSA( FLAGSA_PREFIX_SPACE ))
	bit	3, c
	jr	Z, 00202$
;printf_large_mod.c:450: OUTPUT_CHAR( ' ', p );
	push	bc
	push	de
	ld	a, #0x20
	call	__output_char
	pop	de
	pop	bc
;printf_large_mod.c:452: width--;
	ld	-7 (ix), d
00202$:
;printf_large_mod.c:457: if (!IS_FLAGSA( FLAGSA_LEFT_JUSTIFY ))
	ld	-2 (ix), e
	ld	a, e
	or	a, a
	jr	NZ, 00210$
;printf_large_mod.c:459: while ( width-- > length )
	ld	a, c
	rrca
	and	a, #0x01
	ld	e, a
	ld	c, -7 (ix)
00203$:
	ld	d, c
	dec	c
	ld	a, b
	sub	a, d
	jr	NC, 00317$
;printf_large_mod.c:461: OUTPUT_CHAR( IS_FLAGSA( FLAGSA_ZERO_PADDING ) ? '0' : ' ', p );
	ld	a, e
	or	a, a
	ld	a, #0x30
	jr	NZ, 00237$
	ld	a, #0x20
00237$:
	push	bc
	push	de
	call	__output_char
	pop	de
	pop	bc
	jr	00203$
00210$:
;printf_large_mod.c:467: if (width > length)
	ld	a, b
	sub	a, -7 (ix)
	jr	NC, 00207$
;printf_large_mod.c:468: width -= length;
	ld	a, -7 (ix)
	sub	a, b
	ld	c, a
	jr	00309$
00207$:
;printf_large_mod.c:470: width = 0;
	ld	c, #0x00
;printf_large_mod.c:504: return charsOutputted;
;printf_large_mod.c:474: while( length-- )
00317$:
00309$:
	ld	e, -4 (ix)
	ld	d, -3 (ix)
00215$:
	ld	a, b
	dec	b
	or	a, a
	jr	Z, 00217$
;printf_large_mod.c:476: lsd = !lsd;
	ld	a, -1 (ix)
	xor	a, #0x01
	ld	-1 (ix), a
;printf_large_mod.c:477: if (!lsd)
	bit	0, -1 (ix)
	jr	NZ, 00213$
;printf_large_mod.c:479: pstore++;
	inc	de
;printf_large_mod.c:480: value.byte[4] = *pstore >> 4;
	ld	a, (de)
	swap	a
	and	a, #0x0f
	ld	(#(_value + 4)),a
	jr	00214$
00213$:
;printf_large_mod.c:484: value.byte[4] = *pstore & 0x0F;
	ld	a, (de)
	and	a, #0x0f
	ld	(#(_value + 4)),a
00214$:
;printf_large_mod.c:486: output_digit( value.byte[4] );
	ld	a, (#(_value + 4) + 0)
	push	bc
	push	de
	call	_output_digit
	pop	de
	pop	bc
	jr	00215$
00217$:
;printf_large_mod.c:488: if (IS_FLAGSA( FLAGSA_LEFT_JUSTIFY ) )
	ld	a, -2 (ix)
	or	a, a
	jp	Z, 00231$
;printf_large_mod.c:490: while (width-- > 0)
00218$:
	ld	a, c
	dec	c
	or	a, a
	jp	Z, 00231$
;printf_large_mod.c:492: OUTPUT_CHAR(' ', p);
	push	bc
	ld	a, #0x20
	call	__output_char
	pop	bc
	jr	00218$
00229$:
;printf_large_mod.c:500: OUTPUT_CHAR( c, p );
	call	__output_char
	jp	00231$
00233$:
;printf_large_mod.c:504: return charsOutputted;
	ld	a, (_charsOutputted)
	ld	e, a
	ld	hl, #_charsOutputted + 1
	ld	d, (hl)
;printf_large_mod.c:505: }
	ld	sp, ix
	pop	ix
	pop	hl
	pop	af
	pop	af
	jp	(hl)
___str_0:
	.ascii "<NO FLOAT>"
	.db 0x00
	.area _CODE
	.area _INITIALIZER
	.area _CABS (ABS)
