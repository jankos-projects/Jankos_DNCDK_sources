/*-----------------------------------------------------------------
   printf_large_mod.c

   Copyright (C) 2024:  Janko Stamenović modifications for
                Janko's Dot NextZXOS C Development Kit library

   modified from:
   printf_large.c - formatted output conversion


   Copyright (C) 1999, Martijn van Balen <aed AT iae.nl>

   Added %f By - <johan.knol AT iduna.nl> (2000)

   Refactored by - Maarten Brock (2004)

   This library is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by the
   Free Software Foundation; either version 2, or (at your option) any
   later version.

   This library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this library; see the file COPYING. If not, write to the
   Free Software Foundation, 51 Franklin Street, Fifth Floor, Boston,
   MA 02110-1301, USA.

   As a special exception, if you link this library with other files,
   some of which are compiled with SDCC, to produce an executable,
   this library does not by itself cause the resulting executable to
   be covered by the GNU General Public License. This exception does
   not however invalidate any other reasons why the executable file
   might be covered by the GNU General Public License.
-------------------------------------------------------------------------*/


#include <stdarg.h>
#include <string.h>
#include <ctype.h>
#include <stdio.h>
//#include <stdbit.h>
//#include <sdcc-lib.h>

#define PTR value.ptr

#define bool _Bool

/****************************************************************************/

//typedef const char * ptr_t;
#define ptr_t const char *

#ifdef toupper
#undef toupper
#endif
#ifdef tolower
#undef tolower
#endif
#ifdef islower
#undef islower
#endif
#ifdef isdigit
#undef isdigit
#endif

//#define toupper(c) ((c)&=~0x20)
#define toupper(c) ((c)&=0xDF)
#define tolower(c) ((c)|=0x20)
#define islower(c) ((unsigned char)c >= (unsigned char)'a' && (unsigned char)c <= (unsigned char)'z')
#define isdigit(c) ((unsigned char)c >= (unsigned char)'0' && (unsigned char)c <= (unsigned char)'9')

typedef union
{
  unsigned char  byte[5];
  long           l;
  unsigned long  ul;
  float          f;
  const char     *ptr;
} value_t;

  static bool lower_case;
  static pfn_outputchar output_char;
  static void* p;
  static value_t value;
  static int charsOutputted;

/****************************************************************************/

  #define OUTPUT_CHAR(c, p) _output_char (c)
  static void
  _output_char (unsigned char c)
  {
    output_char( c, p );
    charsOutputted++;
  }

/*--------------------------------------------------------------------------*/

  static void
  output_digit (unsigned char n)
  {
    register unsigned char c = n + (unsigned char)'0';

    if (c > (unsigned char)'9')
    {
      c += (unsigned char)('A' - '0' - 10);
      if (lower_case)
         c = tolower(c);
    }
    _output_char( c );
  }

/*--------------------------------------------------------------------------*/

  #define OUTPUT_2DIGITS( B )   output_2digits( B )
  static void
  output_2digits (unsigned char b)
  {
    output_digit( b>>4   );
    output_digit( b&0x0F );
  }

/*--------------------------------------------------------------------------*/

static void
calculate_digit (unsigned char radix)
{
  register unsigned long ul = value.ul;
  register unsigned char b4 = value.byte[4];
  register unsigned char i = 32;

  do
  {
    b4 = (b4 << 1);
    b4 |= (ul >> 31) & 0x01;
    ul <<= 1;

    if (radix <= b4 )
    {
      b4 -= radix;
      ul |= 1;
    }
  } while (--i);
  value.ul = ul;
  value.byte[4] = b4;
}


enum {
    FLAGSA_LEFT_JUSTIFY = (1 << 0),
    FLAGSA_ZERO_PADDING = (1 << 1),
    FLAGSA_PREFIX_SIGN = (1 << 2),
    FLAGSA_PREFIX_SPACE = (1 << 3),
    FLAGSA_SIGNED_ARGUMENT = (1 << 4),
    FLAGSA_CHAR_ARGUMENT = (1 << 5),
    FLAGSA_LONG_ARGUMENT = (1 << 6),
    FLAGSA_FLOAT_ARGUMENT = (1 << 7),

    DECIMALS_NONE = 255,
};

#define IS_FLAGSA( x ) ((flags_a & (x))!=0)
#define SET_FLAGSA( x ) (flags_a |= (x))
#define CLEAR_FLAGSA( x ) (flags_a &= (~(x)))

int
_print_format (pfn_outputchar pfn, void* pvoid, const char *format, va_list ap)
{
    unsigned char flags_a;
  bool   lsd;

  unsigned char radix;
  unsigned char  width;
  unsigned char  decimals;
  unsigned char  length;
  char           c;

  output_char = pfn;
  p = pvoid;

  // reset output chars
  charsOutputted = 0;

  while( c=*format++ )
  {
    if ( c=='%' )
    {
        flags_a = 0;

      radix           = 0;
      width           = 0;
      decimals        = DECIMALS_NONE;

get_conversion_spec:

      c = *format++;

      if (c=='%')
      {
        OUTPUT_CHAR(c, p);
        continue;
      }

      if (isdigit(c))
      {
        if (decimals==DECIMALS_NONE)
        {
          width = 10*width + c - '0';
          if (width == 0)
          {
            /* first character of width is a zero */
              SET_FLAGSA( FLAGSA_ZERO_PADDING );
          }
        }
        else
        {
          decimals = 10*decimals + c - '0';
        }
        goto get_conversion_spec;
      }

      if (c=='.')
      {
        if (decimals==DECIMALS_NONE)
          decimals=0;
        else
          ; // duplicate, ignore
        goto get_conversion_spec;
      }

      if (islower(c))
      {
        c = toupper(c);
        lower_case = 1;
      }
      else
        lower_case = 0;

      switch( c )
      {
      case '-':
        SET_FLAGSA( FLAGSA_LEFT_JUSTIFY );
        goto get_conversion_spec;
      case '+':
          SET_FLAGSA( FLAGSA_PREFIX_SIGN );
        goto get_conversion_spec;
      case ' ':
          SET_FLAGSA( FLAGSA_PREFIX_SPACE );
        goto get_conversion_spec;
      case 'B': /* byte */
          SET_FLAGSA( FLAGSA_CHAR_ARGUMENT );
        goto get_conversion_spec;
//      case '#': /* not supported */
      case 'H': /* short */
      case 'J': /* intmax_t */
      case 'T': /* ptrdiff_t */
      case 'Z': /* size_t */
        goto get_conversion_spec;
      case 'L': /* long */
          SET_FLAGSA( FLAGSA_LONG_ARGUMENT );
        goto get_conversion_spec;

      case 'C':
          c = va_arg(ap,int);
        OUTPUT_CHAR( c, p );
        break;

      case 'S':
        PTR = va_arg(ap,ptr_t);

        length = strlen(PTR);
        if ( decimals == DECIMALS_NONE )
        {
          decimals = length;
        }
        if ( ( !IS_FLAGSA(FLAGSA_LEFT_JUSTIFY) ) && (length < width) )
        {
          width -= length;
          while( width-- != 0 )
          {
            OUTPUT_CHAR( ' ', p );
          }
        }

        while ( (c = *PTR)  && (decimals-- > 0))
        {
          OUTPUT_CHAR( c, p );
          PTR++;
        }

        if ( IS_FLAGSA(FLAGSA_LEFT_JUSTIFY) && (length < width))
        {
          width -= length;
          while( width-- != 0 )
          {
            OUTPUT_CHAR( ' ', p );
          }
        }
        break;

      case 'P':
        PTR = va_arg(ap,ptr_t);

#if __STDC_ENDIAN_NATIVE__ == __STDC_ENDIAN_BIG__
        OUTPUT_CHAR('0', p);
        OUTPUT_CHAR('x', p);
        OUTPUT_2DIGITS( value.byte[0] );
        OUTPUT_2DIGITS( value.byte[1] );
#else
        OUTPUT_CHAR('0', p);
        OUTPUT_CHAR('x', p);
        OUTPUT_2DIGITS( value.byte[1] );
        OUTPUT_2DIGITS( value.byte[0] );
#endif
        break;

      case 'D':
      case 'I':
          SET_FLAGSA( FLAGSA_SIGNED_ARGUMENT );
        radix = 10;
        break;

      case 'O':
        radix = 8;
        break;

      case 'U':
        radix = 10;
        break;

      case 'X':
        radix = 16;
        break;

      case 'F':
      SET_FLAGSA( FLAGSA_FLOAT_ARGUMENT );
        break;

      default:
        // nothing special, just output the character
        OUTPUT_CHAR( c, p );
        break;
      }

      if ( IS_FLAGSA( FLAGSA_FLOAT_ARGUMENT ) )
      {
        value.f = va_arg(ap, float);
        PTR="<NO FLOAT>";
        while (c=*PTR++)
        {
          OUTPUT_CHAR (c, p);
        }
        // treat as long hex
        //radix=16;
        //long_argument=1;
        //zero_padding=1;
        //width=8;
      }
      else if (radix != 0)
      {
        // Apparently we have to output an integral type
        // with radix "radix"
        static unsigned char store[6];
        unsigned char *pstore = &store[5];

        // store value in byte[0] (LSB) ... byte[3] (MSB)
        if (IS_FLAGSA( FLAGSA_CHAR_ARGUMENT ) )
        {
          value.l = va_arg(ap, char);
          if (!!IS_FLAGSA( FLAGSA_SIGNED_ARGUMENT ) )
          {
            value.l &= 0xFF;
          }
        }
        else if (IS_FLAGSA( FLAGSA_LONG_ARGUMENT ))
        {
          value.l = va_arg(ap, long);
        }
        else // must be int
        {
          value.l = va_arg(ap, int);
          if (!IS_FLAGSA( FLAGSA_SIGNED_ARGUMENT ) )
          {
            value.l &= 0xFFFF;
          }
        }

        if ( IS_FLAGSA( FLAGSA_SIGNED_ARGUMENT ) )
        {
          if (value.l < 0)
            value.l = -value.l;
          else
          CLEAR_FLAGSA( FLAGSA_SIGNED_ARGUMENT );
        }

        length=0;
        lsd = 1;

        do {
          value.byte[4] = 0;
          calculate_digit(radix);
          if (!lsd)
          {
            *pstore = (value.byte[4] << 4) | (value.byte[4] >> 4) | *pstore;
            pstore--;
          }
          else
          {
            *pstore = value.byte[4];
          }
          length++;
          lsd = !lsd;
        } while( value.ul );

        if (width == 0)
        {
          // default width. We set it to 1 to output
          // at least one character in case the value itself
          // is zero (i.e. length==0)
          width = 1;
        }

        /* prepend spaces if needed */
        if (!IS_FLAGSA( FLAGSA_ZERO_PADDING ) && !IS_FLAGSA( FLAGSA_LEFT_JUSTIFY ))
        {
          while ( width > (unsigned char) (length+1) )
          {
            OUTPUT_CHAR( ' ', p );
            width--;
          }
        }

        if (IS_FLAGSA( FLAGSA_SIGNED_ARGUMENT )) // this now means the original value was negative
        {
          OUTPUT_CHAR( '-', p );
          // adjust width to compensate for this character
          width--;
        }
        else if (length != 0)
        {
          // value > 0
          if (IS_FLAGSA( FLAGSA_PREFIX_SIGN ))
          {
            OUTPUT_CHAR( '+', p );
            // adjust width to compensate for this character
            width--;
          }
          else if (IS_FLAGSA( FLAGSA_PREFIX_SPACE ))
          {
            OUTPUT_CHAR( ' ', p );
            // adjust width to compensate for this character
            width--;
          }
        }

        /* prepend zeroes/spaces if needed */
        if (!IS_FLAGSA( FLAGSA_LEFT_JUSTIFY ))
        {
          while ( width-- > length )
          {
            OUTPUT_CHAR( IS_FLAGSA( FLAGSA_ZERO_PADDING ) ? '0' : ' ', p );
          }
        }
        else
        {
          /* spaces are appended after the digits */
          if (width > length)
            width -= length;
          else
            width = 0;
        }

        /* output the digits */
        while( length-- )
        {
          lsd = !lsd;
          if (!lsd)
          {
            pstore++;
            value.byte[4] = *pstore >> 4;
          }
          else
          {
            value.byte[4] = *pstore & 0x0F;
          }
          output_digit( value.byte[4] );
        }
        if (IS_FLAGSA( FLAGSA_LEFT_JUSTIFY ) )
        {
          while (width-- > 0)
          {
            OUTPUT_CHAR(' ', p);
          }
        }
      }
    }
    else
    {
      // nothing special, just output the character
      OUTPUT_CHAR( c, p );
    }
  }

  return charsOutputted;
}


