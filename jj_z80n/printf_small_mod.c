/*-----------------------------------------------------------------
    printf_small_mod.c

    Copyright (C) 2024:  Janko Stamenović modifications for
                Janko's Dot NextZXOS C Development Kit library

    modified from:
    printfl.c - source file for reduced version of printf

    Copyright (C) 1999, Sandeep Dutta . sandeep.dutta@usa.net

    2001060401: Improved by was@icb.snz.chel.su


    This library is free software; you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by the
    Free Software Foundation; either version 2, or (at your option) any
    later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this library; see the file COPYING. If not, write to the
    Free Software Foundation, 51 Franklin Street, Fifth Floor, Boston,
    MA 02110-1301, USA.

    As a special exception, if you link this library with other files,
    some of which are compiled with SDCC, to produce an executable,
    this library does not by itself cause the resulting executable to
    be covered by the GNU General Public License. This exception does
    not however invalidate any other reasons why the executable file
    might be covered by the GNU General Public License.
-------------------------------------------------------------------------*/

/* following formats are supported :-
   format     output type       argument-type
     %d        decimal             int
     %ld       decimal             long
     %hd       decimal             char
     %X        hexadecimal         int
     %lX       hexadecimal         long
     %hX       hexadecimal         char
     %o        octal               int
     %lo       octal               long
     %ho       octal               char
     %c        character           char
     %s        character           generic pointer
*/

#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>

static __data char radix ;
static __data char flags = 0;
static char * __data str ;
static __data long val;

enum { flag_char = 1, flag_long = 2 };

void printf_small (char * fmt, ... ) __reentrant
{
    va_list ap ;

    va_start(ap,fmt);

    for (; *fmt ; fmt++ ) {
        if (*fmt == '%') {
            char tflags = 0;
            fmt++ ;
            switch (*fmt) {
            case 'l': tflags = flag_long; break;
            case 'h': tflags = flag_char; break;
            default: break;
            }
            if (tflags)
                fmt++;
            flags = tflags;
            char c=*fmt;
            char tradix = 0;
            switch (c) {
            case 's': {
                char* tstr = va_arg(ap, char *);
                while (*tstr) putchar(*tstr++);
                continue ;
                }
            case 'd': tradix = 10; break;
            case 'X': tradix = 16; break; // capital as ltoa does capitals
            case 'o': tradix = 8; break;
            case 'c': break;
            default:
                goto PrintFromFmt;
                break;
            }
            radix = tradix;
            char t2flags = flags;
            switch (t2flags) {
            case flag_long:  val = va_arg(ap,long); break;
            case flag_char:  val = va_arg(ap,char); break;
            default: val = va_arg(ap,int); break;
            }
            if (radix)
            {
              static char __idata buffer[12]; /* 37777777777(oct) */
              char __idata * stri;

              __ltoa (val, buffer, radix);
              stri = buffer;
              while (*stri)
                {
                  putchar (*stri);
                  stri++;
                }
            }
            else
              putchar((char)val);

        } else {
PrintFromFmt:
            putchar(*fmt);
        }
    }
}
