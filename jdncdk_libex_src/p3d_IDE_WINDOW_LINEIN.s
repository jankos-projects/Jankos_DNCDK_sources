; part of Janko Stamenović's Janko's Dot NextZXOS C Development Kit library
;
; this file is released under the unlicense:
; unlicense.org (practically public domain)
;
; Note: the library as a whole is licensed differently
;
        .module p3d_IDE_WINDOW_LINEIN_
        .globl _p3d_IDE_WINDOW_LINEIN
        .area _CODE


; regs: ABCDE
; returns: size of the entered input
;extern unsigned char p3d_IDE_WINDOW_LINEIN( const char *buffer, const char* regs );
_p3d_IDE_WINDOW_LINEIN::
        push de
        push hl
        ex de, hl
        ld a, (hl)
        inc hl
        ld b, (hl)
        inc hl
        ld c, (hl)
        inc hl
        ld d, (hl)
        inc hl
        ld e, (hl)
        pop hl
        exx
        ld de, #0x01c3
        ld c, #7
        rst     #0x8
        .db #0x94 ; M_P3DOS
        pop hl
        ld (hl), a
        inc hl
        ld (hl), b
        inc hl
        ld (hl), c
        inc hl
        ld (hl), d
        ld a, e  ; number of chars returned
        ret
