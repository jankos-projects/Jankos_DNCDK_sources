#!/bin/sh

THISLIB=jj_libex

#shellcheck source=./../common.sh.inc
. "$(dirname "$0")"/../common.sh.inc

jjbegin

# cc
jjcc setupuart_0_12_13_14
jjcc setupuart_115200

jjcc  set_mmuslot0
jjcc  set_mmuslot1
jjcc  set_mmuslot2
jjcc  set_mmuslot3
jjcc  set_mmuslot4
jjcc  set_mmuslot5
jjcc  set_mmuslot6
jjcc  set_mmuslot7

jjcc  set_nextreg_6
jjcc  set_nextreg_7
jjcc  is_edit_key_pressed
jjcc  is_break_key_pressed
jjcc  ROM3_0C0A_PO_MSG
jjcc  ROM3_22DF_PLOT
jjcc  esxdos_DISK_FILEMAP
jjcc  esxdos_M_DRVAPI
jjcc  is_IDE_MODE_non0
jjcc  drv_ESPAT_on

# as

jjasm  p3d_IDE_MODE_bytes
jjasm  p3d_IDE_WINDOW_LINEIN
jjasm  p3d_IDE_WINDOW_STRING
jjasm  ROM3_1601_CHAN_OPEN

jjend

