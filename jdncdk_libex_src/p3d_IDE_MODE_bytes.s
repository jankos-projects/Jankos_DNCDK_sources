;  p3d_IDE_MODE_bytes.s - an IDE_MODE call API implementation
;     for
;     Janko's Dot NextZXOS C Development Kit library
;
;   The MIT License
;
; Copyright © 2024 Janko Stamenović
;
; Permission is hereby granted, free of charge, to any person
; obtaining a copy of this software and associated documentation
; files (the “Software”), to deal in the Software without
; restriction, including without limitation the rights to use, copy,
; modify, merge, publish, distribute, sublicense, and/or sell copies
; of the Software, and to permit persons to whom the Software is
; furnished to do so, subject to the following conditions:
;
; The above copyright notice and this permission notice shall be
; included in all copies or substantial portions of the Software.
;
; THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY
; KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
; WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE
; AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
; HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
; WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
; OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
; DEALINGS IN THE SOFTWARE.
;
        .module p3d_IDE_MODE_bytes_
        .globl _p3d_IDE_MODE_bytes
        .area _CODE


; bytes are 9 bytes total: ABCDEHLIX
; input A 0 query, 1 change mode to BC, where
;            B e 0,1,2 (layer) and if B==1
;            C submode:
;                  0=lores, 1=ula, 2=hires, 3=hicol
; output 1 OK
;        0 error
;  if query, bytes changed to these meanings:
;    0   A 0000llhh where hh 0..2 layer, ll submode
;    1   B charwidth px : 3..8
;    2   C flags 00HW000h
;                HW double width/height, h reduced h
;    3   D current paper for lores or L2
;    4   E current attr for L0;ula,hires,hicolor, or
;           current ink for lores or L2
;    5   H "printable" lines: e.g. 22 or less for L0
;             12 / 24 lo-res / ula,hires,hicol,L2
;          when h (reduced height):
;             16 / 32 lo-res / ula,hires,hicol,L2
;         NOT IMPACTED by double height, user
;              must then calc / adjust to actual!
;    6   L "printable columns": e.g.
;             32 L0,   16..170 other modes
;    7,8 IX -- "mode window handle", for L!=0
;       esx puts that in alt HL and preserves ix
;
;extern BOOL p3d_IDE_MODE_bytes( unsigned char* bytes );
_p3d_IDE_MODE_bytes::
        push hl  ; ptr
        ld a, (hl)
        cp a, #2
        jr NC, Fail2
        inc hl
        ld b, (hl)
        inc hl
        ld c, (hl)
        exx
        ld hl, #0 ; set "IX" as 0
        ld de, #0x01d5
        ld c, #7
        rst     #0x8
        .db #0x94 ; M_P3DOS
        jr NC, Fail2
        ; .db 0xdd, 0x1
        ex (sp), hl ; HL:=ptr,(sp):=GOTHL
        ld (hl), a
        inc hl
        ld (hl), b
        inc hl
        ld (hl), c
        inc hl
        ld (hl), d
        inc hl
        ld (hl), e
        inc hl
        pop bc    ; GOTHL
        ld (hl), b
        inc hl
        ld (hl), c
        inc hl
        exx
        push hl  ; in dot IX is in alt HL
        exx
        pop bc
        ld (hl), b
        inc hl
        ld (hl), c
        inc hl
        ld  a, #1
        ret
Fail2:
        pop  hl
        xor a
        ret
