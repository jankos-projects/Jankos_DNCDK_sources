; part of Janko Stamenović's Janko's Dot NextZXOS C Development Kit library
;
; this file is released under the unlicense:
; unlicense.org (practically public domain)
;
; Note: the library as a whole is licensed differently

        .module p3d_IDE_WINDOW_STRING_
        .globl _p3d_IDE_WINDOW_STRING
        .area _CODE

; input:
;     HL= string addr (whole string at addtr <= $c000 !!!)
;     E=
;           $ff == string terminated with ff
;           $80 == last char of string has bit 7
;           0..127 n chars in string (ff still terminates before!)
;
;      call ROM3: $1601 and also
;     in some scenarios CURCHL values have to be specially
;     corrdinated with the use of this function!
;
;extern void p3d_IDE_WINDOW_STRING( const char *s, unsigned term_cond );
_p3d_IDE_WINDOW_STRING::
        exx
        ld de, #0x01c6
        ld c, #7
        rst     #0x8
        .db #0x94 ; M_P3DOS
        ret
