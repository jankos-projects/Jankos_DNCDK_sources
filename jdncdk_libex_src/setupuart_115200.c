/* setupuart_115200.c       part of Janko Stamenović's Janko's Dot NextZXOS C Development Kit library

    The MIT License

Copyright © 2024 Janko Stamenović

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the “Software”), to deal in the Software without
restriction, including without limitation the rights to use, copy,
modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY
KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE
AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
DEALINGS IN THE SOFTWARE.
*/

#include <jj_lib.h>
#include <jj_hardware.h>

const unsigned short prescaler_low_high_115200[] = {
    0x8173, 0x8178, 0x817f, 0x8204, 0x820d, 0x8215, 0x821e, 0x816a, // HILO (0) 115200
};

void setupuart_115200( char b_is_pi )
{
    UART_SELECT = ( b_is_pi ) ? (0x10 | 0x40) : 0x10;
    unsigned char speed = J_readnextreg(0x11) & 0x07;
    unsigned char lo;
    unsigned char hi;
    unsigned hilo = prescaler_low_high_115200[ speed ];
    hi = (unsigned char)( hilo >> 8 );
    lo = (unsigned char)hilo;
    UART_RX = hi;
    UART_RX = lo;
}
