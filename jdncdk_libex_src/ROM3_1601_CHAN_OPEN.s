; part of Janko Stamenović's Janko's Dot NextZXOS C Development Kit library
;
; this file is released under the unlicense:
; unlicense.org (practically public domain)
;
; Note: the library as a whole is licensed differently
;
        .module ROM3_1601_CHAN_OPEN_
        .globl _ROM3_1601_CHAN_OPEN
        .area _CODE


;extern BOOL ROM3_1601_CHAN_OPEN( unsigned char* bytes );
_ROM3_1601_CHAN_OPEN::
        rst #0x18 ; call 48K
        .dw #0x1601
        ret
