#include <jj_libex.h>

_Bool esxdos_DISK_FILEMAP( S_regpairs* r ) __naked
{
    // returns 1 if NO error
    //
    // can continue filling the entries
    // after the previous call if all were
    // filed previously

    // input A filehandle
    //        HL p_6Bentries (>= 0x4000)
    //        DE max_6Bentries
    // output A cardflags/error,
    //        DE max_6Bentries-n_returned
    //        HL end_addr_after_last_returned
    //
    //  cardflags bits:
    //      000000Bi:  S: sector_addressing
    //                 i: card_id
    //
    // each 6B entry: 4B card_address
    //                2B nitems_in_entry
    //
    //        item:   1B              if S==0
    //                512B (a sector) if S==1
    //
    //
    //   the next non-fragmented address is then:
    //   card address + nitems_in_entry * (S ? 512 : 1)

    __asm
    push hl
    ld a, (hl)
    inc hl
    inc hl
    inc hl
    ld e, (hl)
    inc hl
    ld d, (hl)
    inc hl
    ld c, (hl)
    inc hl
    ld h, (hl)
    ld l, c
    rst     #0x8
    .db     #0x85
    ld c, l
    ld b, h
    pop hl
    ld (hl), a
    inc hl
    inc hl
    inc hl
    ld (hl), e
    inc hl
    ld (hl), d
    inc hl
    ld (hl), c
    inc hl
    ld (hl), b
    ld a, #0
    ccf
    rla
    ret
    __endasm;
    r;
}

