// part of Janko Stamenović's Janko's Dot NextZXOS C Development Kit library
//
// this file is released under the unlicense:
// unlicense.org (practically public domain)
//
// Note: the library as a whole is licensed differently

#include <jj_libex.h>

void ROM3_0C0A_PO_MSG( char* s )
{
    __asm
    xor a
    ex de, hl
    rst #0x18
    .dw 0xc0a
    __endasm;
    s;
}
