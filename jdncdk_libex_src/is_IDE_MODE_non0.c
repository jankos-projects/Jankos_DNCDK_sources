#include <jj_libex.h>

unsigned char is_IDE_MODE_non0( void ) __naked
{
    // returns 1 "layer" >= 1
    //
    __asm
    xor a
    exx
    ld  hl, #0 ; set "IX" as 0
    ld  de, #0x01d5 ; call p3d IDE_MODE
    ld  c, #7
    rst #0x8
    .db #0x94 ; M_P3DOS
    jr  NC, Fail2
    and a,#3
    ret
Fail2:
    xor a
    ret
    __endasm;
}

