#include <jj_libex.h>

_Bool esxdos_M_DRVAPI( S_regpairs* r ) __naked
{
    // returns 1 if NO error
    //
    // input
    //        C driver id  (0 == driver API)
    //        B call id
    //        HL first param
    //        DE second param
    // output
    // if carry (i.e. returned 0 from this call == failure)
    //        A == 0 driver not found
    //        else driver specific error code
    //          or esxDOS error code for driver API
    //

    __asm
    push hl
    inc hl
    ld c, (hl)
    inc hl
    ld b, (hl)
    inc hl
    ld e, (hl)
    inc hl
    ld d, (hl)
    inc hl
    ld a, (hl)
    inc hl
    ld h, (hl)
    ld l, a
    rst     #0x8
    .db     #0x92
    ex (sp), hl
    ld (hl), a
    inc hl
    ld (hl), c
    inc hl
    ld (hl), b
    inc hl
    ld (hl), e
    inc hl
    ld (hl), d
    inc hl
    pop bc
    ld (hl), c
    inc hl
    ld (hl), b
    ld a, #0
    ccf
    rla
    ret
    __endasm;
    r;
}

