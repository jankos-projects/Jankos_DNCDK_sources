// part of Janko Stamenović's Janko's Dot NextZXOS C Development Kit library
//
// this file is released under the unlicense:
// unlicense.org (practically public domain)
//
// Note: the library as a whole is licensed differently

#include <jj_libex.h>

void ROM3_22DF_PLOT( unsigned char x, unsigned char y ) {
    __asm
    ld b, l
    ld c, a
    // B C: y x
    rst #0x18 ; call 48K
    .dw #0x22DF
    __endasm;
    x, y;
}

