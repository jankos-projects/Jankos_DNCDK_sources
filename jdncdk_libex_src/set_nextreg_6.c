// part of Janko Stamenović's Janko's Dot NextZXOS C Development Kit library
//
// this file is released under the unlicense:
// unlicense.org (practically public domain)
//
// Note: the library as a whole is licensed differently
//
#include <jj_libex.h>

void set_nextreg_6( unsigned char x )  __preserves_regs( b, c, d, e, h, l )
{
    // New calling convention: byte var in A
    // Also, since SDCC build 14877 the nextreg
    // instuction is fully handled:
    __asm__ ( "nextreg #6, A" );
    x;
}
