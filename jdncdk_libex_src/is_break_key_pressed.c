// part of Janko Stamenović's Janko's Dot NextZXOS C Development Kit library
//
// this file is released under the unlicense:
// unlicense.org (practically public domain)
//
// Note: the library as a whole is licensed differently

#include <jj_libex.h>
#include <jj_hw_keys.h>

unsigned char is_break_key_pressed( void )
{
    // "BREAK SPACE"
    return ( ( IO_keys_shiftzxcv & 1 ) == 0 )
        && ( ( IO_keys_spacesymmnb & 1 ) == 0 );
}
