#include <jj_libex.h>

_Bool drv_ESPAT_on( void ) __naked
{
    // DRIVER 78,1,0:REM enable IRQ
    //
    // returns 1 if NO error (driver there)
    //
    __asm
    ld  bc, #(1*256)+78 ; 78,1
    ld  hl, #0
    ld  e, l
    ld  d, h
    rst #0x8
    .db #0x92
    ld  a, #0
    ccf
    rla
    ret
    __endasm;
}

