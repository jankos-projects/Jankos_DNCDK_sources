#!/bin/sh

THISLIB=jj_libui

#shellcheck source=./../common.sh.inc
. "$(dirname "$0")"/../common.sh.inc

jjbegin

# cc

jjcc  progbar14
jjcc  progbar30
jjcc  draw_pb_window
jjcc  printchar_n_times
jjcc  my_ui_udgs

jjcc  print_at
jjcc  print_bright
jjcc  print_flash
jjcc  print_ink
jjcc  print_paper
jjcc  print_paper_ink

jjend

