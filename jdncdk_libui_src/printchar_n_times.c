#include "jj_lib.h"
#include "jj_libui.h"

void printchar_n_times( unsigned char c, unsigned char n )
{
    if ( !n ) return;
    do {
        printchar( c );
    } while ( --n );
}
