#include "jj_lib.h"
#include "jj_libui.h"

void print_paper_ink( unsigned char paper, unsigned char ink ) {
    printchar( 17 ); // PAPER 0x11
    printchar( paper );
    print_ink( ink );
}
