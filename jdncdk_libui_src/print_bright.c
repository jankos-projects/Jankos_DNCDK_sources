#include "jj_lib.h"
#include "jj_libui.h"

void print_bright( unsigned char i ) {
    printchar( 19 ); // BRIGHT 0x13
    printchar( i );
}
