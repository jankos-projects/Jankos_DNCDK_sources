/* draw_pb_window.c   part of Janko Stamenović's Janko's Dot NextZXOS C Development Kit library

    The MIT License

Copyright © 2024 Janko Stamenović

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the “Software”), to deal in the Software without
restriction, including without limitation the rights to use, copy,
modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY
KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE
AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
DEALINGS IN THE SOFTWARE.
*/
#include "jj_lib.h"
#include "jj_libex.h"
#include "jj_libui.h"

struct PnWindow pb_window;

static const unsigned char rainbow_colours[] = { 0, 2, 6, 4, 5, 0 };

static void draw_pb_midrow() {
    printchar( 0x90 ); printchar_n_times( ' ', pb_window.w_in ); printchar( 0x91 );
}

static unsigned char print_up_to_n_chars( unsigned char n, const char* s ) {
    if ( n == 0 ) return 0;
    do {
        unsigned char c = *s++;
        if ( !c )
            break;
        printchar( c );
    } while ( --n > 0 );
    return n;
}

void draw_pb_window( void ) {
    unsigned char i_rainbow;
    if ( pb_window.w_in == 0 )
        pb_window.w_in = 20;
    my_ui_udgs_in();
    print_at( pb_window.y, pb_window.x );
    print_paper_ink( 0, 7 ); print_bright( 1 );
    unsigned char m = print_up_to_n_chars( pb_window.w_in-(6-2), pb_window.text );
    printchar_n_times( ' ', m );
    i_rainbow = 0;
    unsigned char i = 5;
    do {
        print_paper( rainbow_colours[ i_rainbow ] );
        i_rainbow++;
        print_ink( rainbow_colours[ i_rainbow ] );
        printchar( 0x94 ); // triangle
    } while ( --i );
    print_paper( 0 );
    printchar( ' ' );
    print_paper_ink( 7, 0 );
    print_at( pb_window.y+1, pb_window.x ); draw_pb_midrow();
    print_at( pb_window.y+2, pb_window.x ); draw_pb_midrow();
    print_at( pb_window.y+3, pb_window.x );
    printchar( 0x92 ); printchar_n_times( '_', pb_window.w_in ); printchar( 0x93 );
    my_ui_udgs_out();
}
