/* progbar30.c         part of Janko Stamenović's Janko's Dot NextZXOS C Development Kit library

    The MIT License

Copyright © 2024 Janko Stamenović

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the “Software”), to deal in the Software without
restriction, including without limitation the rights to use, copy,
modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY
KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE
AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
DEALINGS IN THE SOFTWARE.
*/

#include <string.h>

#include "jj_lib.h"
#include "jj_libui.h"

// the SProgressBar14b counts in 14 bits
// each call to tick_get_cellinc_14b says how much
// the progress bar has to be incremented. The progress
// bar itself can't be bigger than 0x3fff elements.
// --------------------------------------------------------

// to increase the max num of "ticks" we can introduce
// a counter which has to reach 0 before we once "tick"
// the 14 bit progress bar
//
// here we have two cases: if the bits >=14 are all 0
// a 14 bits pb is enough, just treat is so.
// otherwise: we have to fit the highest bit to our
// 14 bit counter


struct SProgressBar30b {
    unsigned y_curr;
    unsigned y_start; // 0..65535
    unsigned char b_small;
} g_pbarL;


static char fits_14_bits( unsigned long* x ) {
    // BEWARE !!! little endian only !!!
    unsigned char* bx = (unsigned char*)x + 1;
    return ( ( *bx++ & 0xc0 ) == 0 && *bx++ == 0 && *bx == 0  );
}

#ifdef PB30_ABORT_ON_2HIGHEST
static char highest_two_z( unsigned long* x ) {
    return ( (unsigned char)( *x >> 24 ) & 0xc0 ) == 0; }
#endif

void set_pb_goal_30b(  unsigned long* goal, int pbn ) {
    static unsigned long pb_g;

    if ( fits_14_bits( goal ) ) {
        g_pbarL.b_small = 1;
        set_pb_goal_14b( *goal & 0x3fff, pbn );
        return;
    }

#ifdef PB30_ABORT_ON_2HIGHEST
    if ( !highest_two_z( goal ) )
        JJ_Exit( 2 );
#endif

    g_pbarL.b_small = 0;

    //pb_g = *goal; but less(!) bytes:
    memcpy( &pb_g, goal, sizeof( pb_g ) );
    g_pbarL.y_start = 1;

    do {
        g_pbarL.y_start <<= 1;
        pb_g >>= 1;
    } while ( !fits_14_bits( &pb_g ) );

    g_pbarL.y_curr = g_pbarL.y_start;
    set_pb_goal_14b( pb_g, pbn );
}

unsigned tick_get_cellinc_30b( void ) {
    if ( g_pbarL.b_small )
        return tick_get_cellinc_14b();
    if ( --g_pbarL.y_curr == 0 ) {
        g_pbarL.y_curr = g_pbarL.y_start;
        return tick_get_cellinc_14b();
    }
    return 0;
}
