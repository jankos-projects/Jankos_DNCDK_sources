/* progbar14.c         part of Janko Stamenović's Janko's Dot NextZXOS C Development Kit library

    The MIT License

Copyright © 2024 Janko Stamenović

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the “Software”), to deal in the Software without
restriction, including without limitation the rights to use, copy,
modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY
KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE
AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
DEALINGS IN THE SOFTWARE.
*/

#include "jj_libui.h"

struct SProgressBar14b {
    int c_ticks,
            goal_ticks,
            n_cells,
            d,  g,  h;
    unsigned char m;
} g_pbar;


static void pb_set_goal_i( int x,  int y ) {
    g_pbar.c_ticks = 0;
    g_pbar.h = y << 1;
    g_pbar.d = g_pbar.h - x;
    g_pbar.g = x << 1;
}

static unsigned char should_move( void ) {
    unsigned char r;
    if ( g_pbar.d >= 0 ) {
        g_pbar.d -= g_pbar.g;
        r = 1;
    } else
        r = 0;
    g_pbar.d+= g_pbar.h;
    return r;
}

void set_pb_goal_14b( int goal, int nc ) {
    g_pbar.goal_ticks = goal;
    g_pbar.n_cells = nc;
    g_pbar.m = ( g_pbar.goal_ticks < g_pbar.n_cells );
    if ( g_pbar.m ) {
        pb_set_goal_i( g_pbar.n_cells, g_pbar.goal_ticks - 1 );
    } else {
        pb_set_goal_i( g_pbar.goal_ticks, g_pbar.n_cells );
    }
}

unsigned tick_get_cellinc_14b( void ) {
    if ( g_pbar.c_ticks == g_pbar.n_cells )
        return 0;
    unsigned char r = should_move();
    if ( !g_pbar.m ) {
        g_pbar.c_ticks += r;
        return r;
    }
    unsigned s = 1;
    ++g_pbar.c_ticks;
    while ( !r ) {
        if ( g_pbar.c_ticks == g_pbar.n_cells )
            break;
        r = should_move();
        s++;
        ++g_pbar.c_ticks;
    }
    return s;
}


