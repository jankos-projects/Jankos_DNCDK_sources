; This file: CC0 by Janko stamenović
; Note: the development kit as a whole is licensed differently

        .area _CODE

        .module ul_rotate_left_ul_21_
        .globl _ul_rotate_left_ul_21

;uint32_t ul_rotate_left_ul_21( uint32_t x ) {
;    return (x << n) | (x >> (32 - n));
_ul_rotate_left_ul_21::

        ;; roll<< 24
        ld a, e
        ld e, d
        ld d, l
        ld l, h
        ld h, a

        jp _ul_rotate_right_ul_3
