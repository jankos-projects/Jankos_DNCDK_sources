; This file: CC0 by Janko stamenović
; Note: the development kit as a whole is licensed differently

        .area _CODE

        .module ul_rotate_left_ul_4_
        .globl _ul_rotate_left_ul_4
        .globl _ul_rotate_left_ul_3

;uint32_t ul_rotate_left_ul_4( uint32_t x ) {
;    return (x << n) | (x >> (32 - n));

_ul_rotate_left_ul_4:
        ld a, h
        rl a
        rl e
        rl d
        rl l
        rl h

_ul_rotate_left_ul_3:
        ld a, h
        rl a
        rl e
        rl d
        rl l
        rl h

        ld a, h
        rl a
        rl e
        rl d
        rl l
        rl h

        ld a, h
        rl a
        rl e
        rl d
        rl l
        rl h

        ret
