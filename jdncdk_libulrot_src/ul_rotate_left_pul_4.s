; This file: CC0 by Janko stamenović
; Note: the development kit as a whole is licensed differently

        .area _CODE

        .module ul_rotate_left_pul_4_
        .globl _ul_rotate_left_pul_4
        .globl _ul_rotate_left_BCDE_4
        .globl _ul_rotate_left_BCDE_3

;uint32_t ul_rotate_left_pul_4( const uint32_t* x ) {
;    return (*x << n) | (*x >> (32 - n));
_ul_rotate_left_pul_4::
        ; ptr in HL

        ld e, (hl)
        inc hl
        ld d, (hl)
        inc hl
        ld c, (hl)
        inc hl
        ld b, (hl)

_ul_rotate_left_BCDE_4:
        ld a, b
        rl a
        rl e
        rl d
        rl c
        rl b

_ul_rotate_left_BCDE_3:
        ld a, b
        rl a
        rl e
        rl d
        rl c
        rl b

        ld a, b
        rl a
        rl e
        rl d
        rl c
        rl b

        ld a, b
        rl a
        rl e
        rl d
        rl c
        rl b

        ld h, b
        ld l, c
        ret
