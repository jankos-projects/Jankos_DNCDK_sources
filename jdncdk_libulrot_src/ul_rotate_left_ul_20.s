; This file: CC0 by Janko stamenović
; Note: the development kit as a whole is licensed differently

        .area _CODE

        .module ul_rotate_left_ul_20_
        .globl _ul_rotate_left_ul_20
        .globl _ul_rotate_right_ul_4
        .globl _ul_rotate_right_ul_3
        .globl _ul_rotate_right_ul_2

;uint32_t ul_rotate_left_ul_20( uint32_t x ) {
;    return (x << n) | (x >> (32 - n));
_ul_rotate_left_ul_20::

        ;; roll<< 24
        ld a, e
        ld e, d
        ld d, l
        ld l, h
        ld h, a

_ul_rotate_right_ul_4:
        ld a, e
        rr a
        rr h
        rr l
        rr d
        rr e

_ul_rotate_right_ul_3:
        ld a, e
        rr a
        rr h
        rr l
        rr d
        rr e

_ul_rotate_right_ul_2:
        ld a, e
        rr a
        rr h
        rr l
        rr d
        rr e

        ld a, e
        rr a
        rr h
        rr l
        rr d
        rr e

        ret
