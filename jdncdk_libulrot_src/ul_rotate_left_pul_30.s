; This file: CC0 by Janko stamenović
; Note: the development kit as a whole is licensed differently

        .area _CODE

        .module ul_rotate_left_pul_30_
        .globl _ul_rotate_left_pul_30

;uint32_t ul_rotate_left_pul_30( const uint32_t* x ) {
;    return (*x << n) | (*x >> (32 - n));
_ul_rotate_left_pul_30::
        ; ptr in HL
        ld e, (hl)
        inc hl
        ld d, (hl)
        inc hl
        ld c, (hl)
        inc hl
        ld b, (hl)

        jp _asm_ul_rotateRight_BCDE_2
