; This file: CC0 by Janko stamenović
; Note: the development kit as a whole is licensed differently

        .area _CODE

        .module ul_rotate_left_ul_9_
        .globl _ul_rotate_left_ul_9

;uint32_t ul_rotate_left_ul_9( uint32_t x ) {
;    return (x << n) | (x >> (32 - n));
_ul_rotate_left_ul_9::

        ;; roll<< 8
        ld a, e
        ld e, h
        ld h, l
        ld l, d
        ld d, a

        ld a, h
        rl a
        rl e
        rl d
        rl l
        rl h

        ret
