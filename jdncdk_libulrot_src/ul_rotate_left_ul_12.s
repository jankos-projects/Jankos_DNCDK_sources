; This file: CC0 by Janko stamenović
; Note: the development kit as a whole is licensed differently

        .area _CODE

        .module ul_rotate_left_ul_12_
        .globl _ul_rotate_left_ul_12

;uint32_t ul_rotate_left_ul_12( uint32_t x ) {
;    return (x << n) | (x >> (32 - n));
_ul_rotate_left_ul_12::

        ;; roll<< 8
        ld a, e
        ld e, h
        ld h, l
        ld l, d
        ld d, a

        jp _ul_rotate_left_ul_4

