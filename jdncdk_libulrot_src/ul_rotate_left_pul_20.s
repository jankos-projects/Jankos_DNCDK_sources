; This file: CC0 by Janko stamenović
; Note: the development kit as a whole is licensed differently

        .area _CODE

        .module ul_rotate_left_pul_20_
        .globl _ul_rotate_left_pul_20
        .globl _asm_ul_rotateRight_BCDE_4
        .globl _asm_ul_rotateRight_BCDE_3
        .globl _asm_ul_rotateRight_BCDE_2

;uint32_t ul_rotate_left_pul_20( const uint32_t* x ) {
;    return (*x << n) | (*x >> (32 - n));
_ul_rotate_left_pul_20::
        ; ptr in HL

        ;; roll<< 24
        ld b, (hl)
        inc hl
        ld e, (hl)
        inc hl
        ld d, (hl)
        inc hl
        ld c, (hl)
        inc hl

_asm_ul_rotateRight_BCDE_4:
        ld a, e
        rr a
        rr b
        rr c
        rr d
        rr e

_asm_ul_rotateRight_BCDE_3:
        ld a, e
        rr a
        rr b
        rr c
        rr d
        rr e

_asm_ul_rotateRight_BCDE_2:
        ld a, e
        rr a
        rr b
        rr c
        rr d
        rr e

        ld a, e
        rr a
        rr b
        rr c
        rr d
        rr e

        ld h, b
        ld l, c
        ret
