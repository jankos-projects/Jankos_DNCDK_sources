; This file: CC0 by Janko stamenović
; Note: the development kit as a whole is licensed differently

        .area _CODE

        .module ul_rotate_left_pul_14_
        .globl _ul_rotate_left_pul_14

;uint32_t ul_rotate_left_pul_14( const uint32_t* x ) {
;    return (*x << n) | (*x >> (32 - n));
_ul_rotate_left_pul_14::
        ; ptr in HL

        ;; roll<< 16
        ld c, (hl)
        inc hl
        ld b, (hl)
        inc hl
        ld e, (hl)
        inc hl
        ld d, (hl)
        inc hl

        ld a, e
        rr a
        rr b
        rr c
        rr d
        rr e

        ld a, e
        rr a
        rr b
        rr c
        rr d
        rr e

        ld h, b
        ld l, c
        ret
