; This file: CC0 by Janko stamenović
; Note: the development kit as a whole is licensed differently

        .area _CODE

        .module ul_rotate_left_ul_17_
        .globl _ul_rotate_left_ul_17

;uint32_t ul_rotate_left_ul_17( uint32_t x ) {
;    return (x << n) | (x >> (32 - n));
_ul_rotate_left_ul_17::

        ;; roll<< 16
        ex de, hl

        ld a, h
        rl a
        rl e
        rl d
        rl l
        rl h

        ret

