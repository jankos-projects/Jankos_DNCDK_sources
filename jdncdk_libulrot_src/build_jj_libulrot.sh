#!/bin/sh

THISLIB=jj_libulrot

#shellcheck source=./../common.sh.inc
. "$(dirname "$0")"/../common.sh.inc

jjbegin

# as

jjasm ul_rotate_left_pul_10
jjasm ul_rotate_left_pul_11
jjasm ul_rotate_left_pul_12
jjasm ul_rotate_left_pul_14
jjasm ul_rotate_left_pul_15
jjasm ul_rotate_left_pul_17
jjasm ul_rotate_left_pul_20
jjasm ul_rotate_left_pul_21
jjasm ul_rotate_left_pul_22
jjasm ul_rotate_left_pul_23
jjasm ul_rotate_left_pul_30
jjasm ul_rotate_left_pul_4
jjasm ul_rotate_left_pul_5
jjasm ul_rotate_left_pul_6
jjasm ul_rotate_left_pul_7
jjasm ul_rotate_left_pul_9

jjasm ul_rotate_left_ul_10
jjasm ul_rotate_left_ul_11
jjasm ul_rotate_left_ul_12
jjasm ul_rotate_left_ul_14
jjasm ul_rotate_left_ul_15
jjasm ul_rotate_left_ul_17
jjasm ul_rotate_left_ul_20
jjasm ul_rotate_left_ul_21
jjasm ul_rotate_left_ul_22
jjasm ul_rotate_left_ul_23
jjasm ul_rotate_left_ul_30
jjasm ul_rotate_left_ul_4
jjasm ul_rotate_left_ul_5
jjasm ul_rotate_left_ul_6
jjasm ul_rotate_left_ul_7
jjasm ul_rotate_left_ul_9

jjend
