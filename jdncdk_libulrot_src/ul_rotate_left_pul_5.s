; This file: CC0 by Janko stamenović
; Note: the development kit as a whole is licensed differently

        .area _CODE

        .module ul_rotate_left_pul_5_
        .globl _ul_rotate_left_pul_5

;uint32_t ul_rotate_left_pul_5( const uint32_t* x ) {
;    return (*x << n) | (*x >> (32 - n));
_ul_rotate_left_pul_5::
        ; ptr in HL

        ;; roll<< 8
        ld d, (hl)
        inc hl
        ld c, (hl)
        inc hl
        ld b, (hl)
        inc hl
        ld e, (hl)

        jp _asm_ul_rotateRight_BCDE_3
