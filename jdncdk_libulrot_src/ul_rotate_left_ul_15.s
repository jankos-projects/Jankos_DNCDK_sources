; This file: CC0 by Janko stamenović
; Note: the development kit as a whole is licensed differently

        .area _CODE

        .module ul_rotate_left_ul_15_
        .globl _ul_rotate_left_ul_15

;uint32_t ul_rotate_left_ul_15( uint32_t x ) {
;    return (x << n) | (x >> (32 - n));
_ul_rotate_left_ul_15::

        ;; roll<< 16
        ex de, hl

        ld a, e
        rr a
        rr h
        rr l
        rr d
        rr e

        ret
