; This file: CC0 by Janko stamenović
; Note: the development kit as a whole is licensed differently

        .area _CODE

        .module ul_rotate_left_pul_9_
        .globl _ul_rotate_left_pul_9

;uint32_t ul_rotate_left_pul_9( const uint32_t* x ) {
;    return (*x << n) | (*x >> (32 - n));
_ul_rotate_left_pul_9::
        ; ptr in HL

        ;; roll<< 8
        ld d, (hl)
        inc hl
        ld c, (hl)
        inc hl
        ld b, (hl)
        inc hl
        ld e, (hl)

        ld a, b
        rl a
        rl e
        rl d
        rl c
        rl b

        ld h, b
        ld l, c
        ret
