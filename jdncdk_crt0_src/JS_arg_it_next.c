//  JS_arg_it_next.c - the source of
//   Janko's Dot NextZXOS C Development Kit library argument parsing routines
//
//     The MIT License
//
// Copyright © 2024 Janko Stamenović
//
// Permission is hereby granted, free of charge, to any person
// obtaining a copy of this software and associated documentation
// files (the “Software”), to deal in the Software without
// restriction, including without limitation the rights to use, copy,
// modify, merge, publish, distribute, sublicense, and/or sell copies
// of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY
// KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE
// AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
// HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
// DEALINGS IN THE SOFTWARE.
#include <jj_lib.h>

static const unsigned char* skip_spaces( const unsigned char* src )
{
    if ( !src ) {
        JJ_next_arg_char = 0;
        return src;
    }
    while ( *src == ' ' )
        src++;
    JJ_next_arg_char = *src;
    return src;
}

// returns the character
unsigned char JS_arg_it_start( void )
{
    JS_use_initial_sp_and_mmu4();
    JJ_arg_iterator = skip_spaces( JJ_system_ro_cmd_bytes );
    JS_use_current_sp_and_mmu4();
    // returning a value avoids prev tail jump
    return JJ_next_arg_char;
}

// returns 1 for success
static unsigned char arg_it_next( void )
{
    // returns 1 if any character was copied
    const unsigned char* src = JJ_arg_iterator;
    unsigned char* t = JJ_current_arg;
    unsigned char r = 0;
    if ( src == 0 || *src == 0 )
        goto Lterm;
    if ( *src == '"' ) {
        src++;
        unsigned char cb = 255;
        do {
            char c = *src;
            if ( c == '"'  ) {
                src++;
                goto Lterm;
            }
            if ( c == 0 || c == '\r' )
                break;
            *t++ = c;
            r = 1;
            src++;
        } while ( --cb != 0 );
        goto LInvalid;
    }
    unsigned char cb = 255;
    do {
        char c = *src;
        if (     c == 0   || c == '\r' || c == ':'
              || c == '"' || c == ' ' )
            goto Lterm;
        *t++ = c;
        src++;
        r = 1;
    } while ( --cb != 0 );
LInvalid:
    JJ_errno = esx_err_invalid_filename;
Lterm:
    JJ_arg_iterator = skip_spaces( src );
    *t = 0;
    return r;
}

// returns 1 for success
unsigned char JS_arg_it_next( void )
{
    JS_use_initial_sp_and_mmu4();
    JJ_b_crt0 = arg_it_next();
    JS_use_current_sp_and_mmu4();
    // returning a value avoids prev tail jump
    return JJ_b_crt0;
}
