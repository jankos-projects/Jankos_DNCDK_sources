;
;  JS_use_sp_and_mmu4.s - the source of the core routines for
;  Janko's Dot NextZXOS C Development Kit library mmu4 page switching
;
;   The MIT License
;
; Copyright © 2024 Janko Stamenović
;
; Permission is hereby granted, free of charge, to any person
; obtaining a copy of this software and associated documentation
; files (the “Software”), to deal in the Software without
; restriction, including without limitation the rights to use, copy,
; modify, merge, publish, distribute, sublicense, and/or sell copies
; of the Software, and to permit persons to whom the Software is
; furnished to do so, subject to the following conditions:
;
; The above copyright notice and this permission notice shall be
; included in all copies or substantial portions of the Software.
;
; THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY
; KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
; WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE
; AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
; HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
; WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
; OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
; DEALINGS IN THE SOFTWARE.
;
        .zxn

        .module use_sp_and_mmu4_
        .globl _JS_use_initial_sp_and_mmu4
        .globl _JS_use_current_sp_and_mmu4
        .area _CODE

; this pair is designed to be used at the entry and exit
; of a carefully(!) constructed function that calls an
; implementation which operates on the initial mmu4
; with the initial sp

;extern void use_initial_sp_and_mmu4( void )

;extern void use_current_sp_and_mmu4( void )

_JS_use_initial_sp_and_mmu4::
        ; switches the mmu4 page and stack at once!
        ;
        ; ; .db 0xdd, 0x01 ; cspect break
        pop     hl     ; hl := ret addr
        ld      (#_JJ_w_crt0),hl
        ld      (#_JJ_current_sp), sp ; save curr SP

        ld      a, #0x54
        call    _J_readnextreg
        ld      (#_JJ_current_mmu4), a ; prev

        ld      a, (#_JJ_initial_mmu4)
        di
        nextreg  #0x54, a ; write to mmu4 reg
        ld      sp, (#_JJ_initial_sp) ; now: initial SP
        ei
        ld      hl,(#_JJ_w_crt0)       ; ret addr
        jp      (hl)                 ; go there


_JS_use_current_sp_and_mmu4::
        ; switches the mmu4 page and stack at once!
        ;
        ; has to be called through a real call,
        ; definitely _not_ as a tail jump.
        ;
        ; We use this part in the same function
        ; F which called use_initial. But at the
        ; moment of the tail jump instead of a call
        ; we still have the "initial" stack and page
        ; even if we entered the F with "current"
        ; so if reached here with a tail jump
        ; to pop the ret address we would not
        ; pop the return address out of F but
        ; whatever is at the "initial" stack now,
        ; and that is surely wrong return address.
        ;
        ; ; .db 0xdd, 0x01 ; cspect break
        pop     hl     ; hl := ret addr
        ld      (#_JJ_w_crt0),hl

        ld      a, (#_JJ_current_mmu4) ; curr
        di
        nextreg  #0x54, a  ; write to mmu4 reg
        ld      sp, (#_JJ_current_sp) ; now: curr sp
        ei
        ld      hl,(#_JJ_w_crt0)      ; ret addr
        jp      (hl)                ; go there
