;
;  jj_crtnano1.s - the source of the CRTnano1 module of
;     Janko's Dot NextZXOS C Development Kit library
;
;   The MIT License
;
; Copyright © 2024 Janko Stamenović
;
; Permission is hereby granted, free of charge, to any person
; obtaining a copy of this software and associated documentation
; files (the “Software”), to deal in the Software without
; restriction, including without limitation the rights to use, copy,
; modify, merge, publish, distribute, sublicense, and/or sell copies
; of the Software, and to permit persons to whom the Software is
; furnished to do so, subject to the following conditions:
;
; The above copyright notice and this permission notice shall be
; included in all copies or substantial portions of the Software.
;
; THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY
; KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
; WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE
; AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
; HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
; WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
; OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
; DEALINGS IN THE SOFTWARE.
;
        .zxn

        .module jj_crtnano1
        .globl  _heap

        .globl  _JJ_end_crt0_vars
        .globl  _J_asm_safe_memset
        .globl  _J_asm_safe_ldir
        .globl  _J_crt_putchar

        .area _HEADER(ABS)
        .org    0x2000
_crt0_entry:
        call    gsinit   ; init all the memory variables
        call    _main
        or      a               ; no err for code 0
        ret     Z               ; A=0, Fc==0 OK
        scf                     ; Fc==1: error code in A
        ret
_J_asm_safe_memset:
        ; input:
        ; A: value, DE: destination, BC: cbytes
        ld      h, a
        ld      a, c
        or      b
        ret     Z
        ld      l, e
        ld      a, h
        ld      h, d
        ld      (hl), a
        inc     de
        dec     bc
_J_asm_safe_ldir:
        ld      a,b
        or      c
        ret     Z
        ldir
        ret
; extern void J_intrinsic_printchar( byte c );
_J_crt_putchar::
        rst     0x10
        ret

_JJ_end_crt0_vars:

        ;;      the first file the linker gets sets
        ;;      the order of area definitions
        .area   _HOME
        .area   _CODE
        .area   _INITIALIZER
        .area   _GSINIT
        .area   _GSFINAL
        .area   _DATA
        .area   _INITIALIZED
        .area   _BSS
        .area   _HEAP

        .area   _GSINIT
gsinit:
        ;; data initialization code
        ld      bc, #l__DATA
        ld      de, #s__DATA
        xor     a
        call    _J_asm_safe_memset
        ld      bc, #l__INITIALIZER
        ld      de, #s__INITIALIZED
        ld      hl, #s__INITIALIZER
        jp      _J_asm_safe_ldir
        .area   _GSFINAL
        ret

        .area   _HEAP
_heap::
