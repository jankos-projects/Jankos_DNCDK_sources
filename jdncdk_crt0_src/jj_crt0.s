;
;  jj_crt0.s - the source of the CRT0 module of
;     Janko's Dot NextZXOS C Development Kit library
;
;   The MIT License
;
; Copyright © 2024 Janko Stamenović
;
; Permission is hereby granted, free of charge, to any person
; obtaining a copy of this software and associated documentation
; files (the “Software”), to deal in the Software without
; restriction, including without limitation the rights to use, copy,
; modify, merge, publish, distribute, sublicense, and/or sell copies
; of the Software, and to permit persons to whom the Software is
; furnished to do so, subject to the following conditions:
;
; The above copyright notice and this permission notice shall be
; included in all copies or substantial portions of the Software.
;
; THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY
; KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
; WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE
; AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
; HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
; WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
; OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
; DEALINGS IN THE SOFTWARE.
;
        .zxn

        .module jj_crt0
        .globl  _heap_m

        .globl  _JJ_system_ro_cmd_bytes
        .globl  _JJ_end_crt0_vars
        .globl  _JJ_osiy
        .globl  _JJ_initial_sp
        .globl  _JJ_current_sp
        .globl  _JJ_initial_mmu4
        .globl  _JJ_current_mmu4
        .globl  _JJ_on_exit_f
        .globl  _JJ_errno
        .globl  _JJ_arg_iterator
        .globl  _JJ_next_arg_char
        .globl  _JJ_current_arg
        .globl  _JJ_b_crt0
        .globl  _JJ_w_crt0
        .globl  _JJ_crt_bits
        .globl  _JJ_b7_cerrmsg
        .globl  _J_memset
        .globl  _J_exit
        .globl  _J_readnextreg
        .globl  _J_IDE_BANK
        .globl  _J_restore_iy
        .globl  _J_crt_ldir
        .globl  _J_crt_putchar
        .globl  _J_asm_safe_memset
        .globl  _J_asm_safe_ldir

        .area _HEADER(ABS)
        .org    0x2000
_crt0_entry:
        ld      (_JJ_system_ro_cmd_bytes),hl
        ld      (_JJ_osiy), iy
        ld      (#_JJ_initial_sp), sp     ; store the initial SP
        rst     #0x8
        .db     #0x88                   ; m_dosversion
        jr      c,bad_nextzxos          ; if Fc: not nextzxos
        jr      nz,bad_nextzxos
        ld      hl,# 256*'N'+'X'
        sbc     hl,bc
        jr      nz,bad_nextzxos         ; wrong signature
        ld      hl,# 0x0202             ; must be BCD encoded
        ex      de,hl                   ; e.g. 0x0194 == 1.94
        sbc     hl,de
        jr      nc,proceed              ; version >= 2.02
bad_nextzxos:
        ld      hl, #msg_bados
        jp      custom_msg_ret
proceed:
        ; alloc next available 8K bank from ZX memory
        ld      hl, #0x0001 ; H: 0: ZX bank, L: 1: rc_bank_alloc
        call    _J_IDE_BANK
        jr      NC, exit_early_allocfail
        ld      hl, #h_my_bank
        ld      (hl), e   ; E has the allocated page

        di
        ld      a, #0x54
        call    _J_readnextreg
        ld      (#_JJ_initial_mmu4), a    ; preserve prev pg
        ld      a, e                 ; the allocated page
        nextreg  #0x54, a
        ld      sp, #0x9ffc          ; SP to the top of page 4
        ei

        ; register dot command error handler:
        ld      hl, #_J_exit
        rst     #0x8
        .db     #0x95 ; m_errh ;

        call    gsinit   ; init all the memory variables
        call    _main
finale:
        ld      d,e        ; retval received in E
        ld      b,e
        push    de         ; save retval
        xor     a
        ld      hl, #_JJ_on_exit_f
        ld      e, (hl)
        ld      (hl), a    ; prevent reentry
        inc     hl
        ld      d, (hl)
        ld      (hl), a    ; prevent reentry
        ex      de, hl
        ld      a, h
        or      l
        ld      a, b        ; pass error value calling
        call    NZ, call_hl ;     _JJ_on_exit_f if != 0
        pop     de    ; stack will go away so pop retval

        di
        ; Important: NO variables except JJ_...s
        ; will exist after this point.
        ;
        ; revert mmu slot 4 to the initial and SP
        ;
        ld      a, (#_JJ_initial_mmu4)
        nextreg  #0x54, a
        ld      sp, (#_JJ_initial_sp) ; the original SP now
        ei

        push    de              ; save retval again

        ld      hl, #h_my_bank
        ld      e, (hl)         ; page to free
        ld      hl, #0x0003     ; H=0 ZX mem. 8K; L=3 FREE
        call    _J_IDE_BANK

        call    _J_restore_iy
        ld      hl, (#_JJ_b7_cerrmsg)
        ld      a, h
        or      l
        jr      nz, ret_cust_err
        pop     af              ; get retval
        or      a               ; no err for code 0
        ret     Z               ; A=0, Fc==0 OK
        scf                     ; Fc==1: error code in A
        ret
ret_cust_err:
        pop     af
        xor     a
        scf
        ret
_J_exit:
        ld      e, a            ; exit
        jr      finale

exit_early_allocfail:
        ld      hl, #msg_allocfail
custom_msg_ret:
        ld      sp,(#_JJ_initial_sp)    ; restore SP
        xor     a               ; A:0 == custom error code in HL
        scf                     ; CF:=1: error
        ; void J_restore_iy( void )
_J_restore_iy:
        ld      iy, (#_JJ_osiy)
        ret
call_hl:
        push     hl
        ret
; extern unsigned char J_readnextreg( unsigned char reg )
;                          __preserves_regs( d, e, h, l );
_J_readnextreg::
        ; input: A  output: A
        ld bc, #0x243B      ; BC destroyed
        out (c), a
        inc b
        in a,(c)
        ret     ; retval is in A

; extern unsigned J_IDE_BANK( unsigned HL, unsigned DE )
_J_IDE_BANK:
        exx                  ; many params in alternates
        ld      de, #0x01bd  ; IDE_BANK
        ld      c, #7        ; bank to page in: here: 7
        rst     #0x8
        .db     #0x94        ; +3dos call
        ld      d, #0  ; D will be 0 for no error
        ret     C      ; and E will be an answer
        ld      d, a   ; D non-zero with error code
        ret

; extern byte* J_crt_ldir( const byte* src, byte* dest, word cb_nonzero );
_J_crt_ldir:
        pop     af
        pop     bc
        push    af
        ldir
        ret

; extern byte* J_memset( byte x, byte* buf, word cb )
_J_memset:
        pop     hl
        pop     bc
        push    hl
_J_asm_safe_memset:
        ; input:
        ; A: value, DE: destination, BC: cbytes
        ld      h, a
        ld      a, c
        or      b
        ret     Z
        ld      l, e
        ld      a, h
        ld      h, d
        ld      (hl), a
        inc     de
        dec     bc
_J_asm_safe_ldir:
        ld      a,b
        or      c
        ret     Z
        ldir
        ret
; extern void J_intrinsic_printchar( byte c );
_J_crt_putchar::
        rst     0x10
        ret
msg_allocfail:
        .ascii  "M.bank alloc fai"
        .db     'l'+#0x80
msg_bados:
        .ascii  "NextZXOS v2.02+ neede"
        .db     'd'+#0x80
        ; crt0 globals
h_my_bank:
        .db     0
_JJ_crt_bits:
        .db     0
_JJ_osiy:
        .word   0
_JJ_initial_sp:
        .word   0
_JJ_current_sp:
        .word   0
_JJ_initial_mmu4:
        .db     0
_JJ_current_mmu4:
        .db     0
_JJ_errno:
        .db     0
_JJ_b_crt0:
        .db     0
_JJ_w_crt0:
        .word   0
_JJ_system_ro_cmd_bytes:
        .word   0
_JJ_b7_cerrmsg:
        .word   0
_JJ_on_exit_f:
        .word   0
_JJ_arg_iterator:
        .word   0
_JJ_next_arg_char:
        .db     0
crt_vars_end_marker:
        .db     0xff

_JJ_end_crt0_vars:

        ;;      the first file the linker gets sets
        ;;      the order of area definitions
        .area   _HOME
        .area   _CODE
        .area   _INITIALIZER
        .area   _GSINIT
        .area   _GSFINAL
        .area   _DATA
        .area   _INITIALIZED
        .area   _BSS
        .area   _HEAP

        .area   _GSINIT
gsinit:
        ;; data initialization code
        ld      bc, #l__DATA
        ld      de, #s__DATA
        xor     a
        call    _J_asm_safe_memset
        ld      bc, #l__INITIALIZER
        ld      de, #s__INITIALIZED
        ld      hl, #s__INITIALIZER
        jp      _J_asm_safe_ldir

        .area   _GSFINAL
        ret

_JJ_current_arg:
        .ds 257
_heap_m::
