
Jankos_DNCDK_source README
==========================


This is Jankos_DNCDK_source, a part of Janko's Dot NextZXOS C
Development Kit v1.0 containing the source files from which the
libraries and .rel files from Janko's Dot NextZXOS C Development
Kit v1.0 could be rebuilt.

The web address on which they are published is currently:

https://gitlab.com/jankos-projects/Jankos_DNCDK_source

These source files depend on rest of the kit, published as:

https://gitlab.com/jankos-projects/Jankos_DNCDK

These source files aren't needed to be able to compile code using
SDCC and its include files and Janko's Dot NextZXOS C Development
Kit include files and libraries, they are provided as a reference.

Users who would like to have a different implementation of any
function which is contained in any of the libraries could simply
implement the function of the same name and conforming to the
existing declaration. The linker will always link the files
provided by the user and use the files from the libraries when the
equivalents aren't already provided.

Users are invited to publish their own software which depends on
SDCC and Janko's Dot NextZXOS C Development Kit, including their
additional functions suitable to the use with the kit or the
alterative implementations of the functions of the kit under
non-restrictive licenses to allow communities to build upon the
foundations enabled by SDCC and this kit.



See LICENSE.txt for the license of the kit.

Content:

jdncdk_crt0_src:
    Sources needed to rebuild jj_crt0 and jj_core.lib from
    Janko's Dot NextZXOS C Development Kit

jdncdk_libex_src:
    Sources needed to rebuild jj_libex.lib from
    Janko's Dot NextZXOS C Development Kit

jdncdk_lib_src:
    Sources needed to rebuild jj_lib.lib from
    Janko's Dot NextZXOS C Development Kit

jj_z80n:
    Sources needed to create a custom build of z80n.lib, a
    library published as a part of SDCC, free open source,
    retargettable, optimizing ISO C compiler for Intel MCS-51 based
    microprocessors (8031, 8032, 8051, 8052, etc.), Maxim (formerly
    Dallas) DS80C390 variants, Freescale (formerly Motorola) HC08
    based (hc08, s08), Zilog Z80 based MCUs (Z80, Z80N, Z180, SM83
    (e.g. Game Boy), Rabbit 2000, Rabbit 2000A/3000, Rabbit 3000A,
    TLCS-90, R800), STMicroelectronics STM8, Padauk PDK14 and PDK15
    and MOS 6502: https://sdcc.sourceforge.net/ licensed under
    GNU General Public License version 2.0

common.sh.inc:
    An include file for shell scripts which build the libraries.
