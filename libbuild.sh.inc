# libbuild.sh.inc  a sh include for building
#                   libraries compatible with JDNCDK
#                   Part of JDNCDK
#
# Copyright (c) 2024 Janko Stamenovic (this file: MIT License)
#
# uses ninja binary

die() {
    echo Error
    exit 1
}

JJ_LIB_ROOT="../../Jankos_DNCDK/jdncdk1"
THISLIB_ROOT=".."

THISNINJA="build/build_$THISLIB.ninja"

jjasm() {
    ALL_ASM="$ALL_ASM \$builddir/$1.rel \$\n"
    echo "build \$builddir/$1.rel: as $1.s" >>"$THISNINJA"
}


jjasmcrt() {
    echo "build \$builddir/$THECRT0.rel: as $THECRT0.s" >>"$THISNINJA"
}


jjasm_sdccls() {
    ALL_ASM="$ALL_ASM \$builddir/$1.rel \$\n"
    echo "build \$builddir/$1.rel: as \$sdcc_lib_src_dir/z80n/$1.s" >>"$THISNINJA"
}


jjcc() {
    ALL_CC="$ALL_CC \$builddir/$1.rel \$\n"
    #echo "build \$builddir/$1.rel: cc3m $1.c" >>"$THISNINJA"
    echo "build \$builddir/$1.rel: cc $1.c" >>"$THISNINJA"
}

jjcc2() {
    ALL_CC="$ALL_CC \$builddir/$1.rel \$\n"
    echo "build \$builddir/$1.rel: cc2 $1.c" >>"$THISNINJA"
}

jjcc3M(){
    ALL_CC="$ALL_CC \$builddir/$1.rel \$\n"
    echo "build \$builddir/$1.rel: cc3m $1.c" >>"$THISNINJA"
}

jjcc_sdccls2() {
    ALL_CC="$ALL_CC \$builddir/$1.rel \$\n"
    echo "build \$builddir/$1.rel: cc2 \$sdcc_lib_src_dir/$1.c" >>"$THISNINJA"
}

jjcc_sdccls() {
    ALL_CC="$ALL_CC \$builddir/$1.rel \$\n"
    echo "build \$builddir/$1.rel: cc \$sdcc_lib_src_dir/$1.c" >>"$THISNINJA"
}


generate_header() {
    printf "\nthislib=$THISLIB.lib\n" >"$THISNINJA"
    printf "
jj_lib_root = $JJ_LIB_ROOT
thislib_root = $THISLIB_ROOT

" >>"$THISNINJA"
printf '

lib = $thislib_root/lib/$thislib

as_opts = -xlosff -g

builddir = build

peepopt = --peep-file $jj_lib_root/include/jj_peep.def


cc_oc = -mz80n --std-c23 $
  --opt-code-size --reserve-regs-iy $
  --Werror --peep-return $
    $peepopt  $
  -I $jj_lib_root/include $
  -I $jj_lib_root/include -c -o


cc_opts = --max-allocs-per-node90000 $cc_oc

cco2 = --max-allocs-per-node25000 $cc_oc

cco3m = --max-allocs-per-node3000000 $cc_oc

rule cp
  command = cp $in $out

rule ar
  command = sdar r $out $in

rule as
  command = sdasz80 $as_opts $out $in

rule runlib
  command = sdrunlib $out

rule cc
  command = sdcc $cc_opts $out $in

rule cc2
  command = sdcc $cco2 $out $in

rule cc3m
  command = sdcc $cco3m $out $in

' >> "$THISNINJA"
}

generate_footer() {
    printf "\nbuild \$lib: ar $ALL_ASM\$\n$ALL_CC\n\n" >>"$THISNINJA"
}


jjbegin() {
    rm -f "$THISLIB_ROOT/lib/$THISLIB.lib"
    [ -d build ] || mkdir build
    rm -f build/*
    generate_header
}

jjbegin_nodel() {
    generate_header
}

jjend() {
    generate_footer
    time ninja $NJOBS -v -f "$THISNINJA" || die
    echo "OK"
}


